<?php

namespace App\Http\Controllers\UserPanel;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Session;
use Illuminate\Http\Request;
use Mail;
use App\Mail\SendMail;
use File;
Use \Carbon\Carbon;
use App\Model\Player;
use App\Model\Benchmark;
use App\Model\Retest;
use App\Model\Goal;
use DB;

class PlayerController extends Controller
{
    public function index(){
        if(Session::has('playerlogin')){
            return redirect('/profile');
        }else{
            return view('UserPanel/login');
        }
    }

    public function test(){
        if(Session::has('playerlogin')){
            $player_id = Session::get('playerlogin')['login_id'] ;
            $playerInfo = DB::select("select * from player where id = '$player_id'");
            $benchmarkInfo = Benchmark::where('player_id',$player_id)->first();
			if($benchmarkInfo){
                $benchmarkInfo = $benchmarkInfo->toArray();
				return view('UserPanel.test',compact('player_id','playerInfo','benchmarkInfo'));
			}else{
				return view('UserPanel.test',compact('player_id'));
			}                      
        }else{
			return redirect('/');
		}
    }


    public function retest(){
        if(Session::has('playerlogin')){
            $player_id = Session::get('playerlogin')['login_id'] ;
            $playerInfo = DB::select("select * from player where id = '$player_id'");
            return view('UserPanel.retest',compact('player_id','playerInfo'));
        }else{
			return redirect('/');
		}
    }

	public function register(){
		return view('UserPanel.register');
	}
	
	public function profile(){
		if(Session::has('playerlogin')){
			$login_id = Session::get('playerlogin')['login_id'] ;
			$playerInfo = DB::select("select * from player where id = '$login_id'");
            $proflBenchmarkInfo = DB::table('benchmark')
                                ->select('benchmark.*','level.level_name' )
                                ->leftJoin('player', 'player.id', '=', 'benchmark.player_id')
                                ->leftJoin('level', 'level.id', '=', 'player.level')
                                ->where('player_id',$login_id)
                                ->get();

            $query = DB::table('retest')->where('player_id',$login_id);
            $retestList = $query->paginate(4);
			if(count($playerInfo) > 0){
				return view('UserPanel.profile',compact('playerInfo','proflBenchmarkInfo','retestList'));
			}else{
				return redirect('/');
			}
		}else{
			return redirect('/');
		}
	}

    public function compareRetest(Request $request){
        $input = $request->all();
        $retest_id = $input['retest_id'];
        $level_id = $input['level'];
        if(Session::has('playerlogin')){
			$login_id = Session::get('playerlogin')['login_id'] ;
			$playerInfo = DB::select("select player.*,level.level_name from player left join level on level.id = player.level where player.id = '$login_id'");
            $benchmarkInfo = Benchmark::where('player_id',$login_id)->first()->toArray();
            $proflBenchmarkInfo = DB::table('benchmark')
                                ->select('benchmark.*','player.level as level_id','level.level_name' )
                                ->leftJoin('player', 'player.id', '=', 'benchmark.player_id')
                                ->leftJoin('level', 'level.id', '=', 'player.level')
                                ->where('player_id',$login_id)
                                ->get();
            $retestInfo = Retest::where('id',$retest_id)->where('player_id',$login_id)->first()->toArray();   
            $goalInfo = Goal::where('level_id',$level_id)->first();              
			if(count($playerInfo) > 0){
                if($goalInfo){
                    $goalInfo = $goalInfo->toArray();
                }else{
                    $goalInfo  = [];
                }
                
				return view('UserPanel.compareRetest',compact('playerInfo','benchmarkInfo','proflBenchmarkInfo','retestInfo','goalInfo'));
			}else{
				return redirect('/');
			}
		}else{
			return redirect('/');
		}
    }

    public function compareBenchmark(){
		if(Session::has('playerlogin')){
			$login_id = Session::get('playerlogin')['login_id'] ;
			$playerInfo = DB::select("select * from player where id = '$login_id'");
            $benchmarkInfo = Benchmark::where('player_id',$login_id)->first();
            $proflBenchmarkInfo = DB::table('benchmark')
                                ->select('benchmark.*','player.level as level_id','level.level_name' )
                                ->leftJoin('player', 'player.id', '=', 'benchmark.player_id')
                                ->leftJoin('level', 'level.id', '=', 'player.level')
                                ->where('player_id',$login_id)
                                ->get();
            $retestInfo = Retest::where('player_id',$login_id)->orderBy('id','DESC')->first();
            if(!$retestInfo){
                $retestInfo['10_yd_sprint'] = 0;
                $retestInfo['40_yd_sprint'] = 0;
                $retestInfo['60_yd_sprint'] = 0;
                $retestInfo['grip_strength'] = 0;
                $retestInfo['broad_jump'] = 0;
                $retestInfo['vertical_jump'] = 0;
                $retestInfo['ss_shoulder_flex'] = 0;
                $retestInfo['pro_agility'] = 0;
                $retestInfo['throwing_velocity'] = 0;
                $retestInfo['exit_velocity'] = 0;
                $retestInfo['stand_and_reach'] = 0;
                $retestInfo['htps'] = 0;
                $retestInfo['inserted_date'] = "";
            }
            $level_id = $proflBenchmarkInfo[0]->level_id;
            $goalInfo = Goal::where('level_id',$level_id)->first();    
			if($benchmarkInfo){                
                $benchmarkInfo = $benchmarkInfo->toArray();
                if(!is_array($retestInfo)){
                    $retestInfo = $retestInfo->toArray();
                }               
				return view('UserPanel.compareBenchmark',compact('playerInfo','benchmarkInfo','proflBenchmarkInfo','retestInfo','goalInfo'));
			}else{
				return redirect('/profile');
			}
		}else{
			return redirect('/');
		}
	}
	
	public function logout()
    {
        session()->forget('playerlogin');
        return redirect('/');
    }

    public function registerTesting(Request $request)
    {
        $input = $request->all();
        
        $rules=[];                      
                   
        $validator = Validator::make($input,$rules); 
        $errors = $validator->errors();
        if($errors->any()){ 
            $error_msgs = $errors->all();
            return ['err_field' => $errors, 'err_msg' => $error_msgs];
        }else{
            DB::beginTransaction();
            try {          							             
                $insert_data = [
                    'player_id' => request('player_id'),
                    '10_yd_sprint' => request('10_yd_sprint') !="" ? request('10_yd_sprint') : 0,
                    '40_yd_sprint' => request('40_yd_sprint') !="" ? request('40_yd_sprint') : 0,
                    '60_yd_sprint' => request('60_yd_sprint') !="" ? request('60_yd_sprint') : 0,
                    'grip_strength' => request('grip_strength') !="" ? request('grip_strength') : 0,                  
                    'broad_jump' => request('broad_jump') !="" ? request('broad_jump') : 0,
                    'vertical_jump' => request('vertical_jump') !="" ? request('vertical_jump') : 0,
                    'ss_shoulder_flex' => request('ss_shoulder_flex') !="" ? request('ss_shoulder_flex') : 0,                  
                    'pro_agility' => request('pro_agility') !="" ? request('pro_agility') : 0,
                    'throwing_velocity' => request('throwing_velocity') !="" ? request('throwing_velocity') : 0,
                    'exit_velocity' => request('exit_velocity') !="" ? request('exit_velocity') : 0,                    
                    'stand_and_reach' => request('stand_and_reach') !="" ? request('stand_and_reach') : 0,
                    'htps' => request('htps') !="" ? request('htps') : 0,                   
                    'inserted_date' => Carbon::now()->setTimezone("America/Toronto")->toDateTimeString(),
                ];
                

                DB::table('benchmark')->insert($insert_data);
								               
                DB::commit();   

                Session::flash('success_msg', 'Your benchmark is updated. Please Click <a href="'.url("/profile").'">GOTO Profile</a> to compare your numbers with your current level.');

                //send mail to admin   
                $login_id = Session::get('playerlogin')['login_id'] ; 
                $playerInfo = Player::where('id',$login_id)->first();                     
                $email_subject = "Notification";
                $sender_name = $playerInfo->first_name.' '.$playerInfo->last_name;
                $fromMail = 'ace@gmail.com';
                $ccMail = 'likhon.colgisbd@gmail.com';
                $toMail = 'ideakeek@gmail.com';          
                $email_content = $playerInfo->first_name.' '.$playerInfo->last_name.' just submitted benchmark.'.'<br>';
                //$email_content .= "Email: ".request('email').'<br>';
                //$email_content .= "Contact number: ".request('phone');                                           
                
                Mail::send(new SendMail($sender_name,$toMail,$fromMail,$email_subject,$email_content,$ccMail));
                if (count(Mail::failures()) > 0) {
                    DB::rollback();
                    return "no_mail";
                }else{
                    return "ok";
                }
                //send mail to admin ends

                return "ok";
            } catch (\Exception $ex) {
                DB::rollback();
                return "ng";
            }
        } 
    }

    public function editBenchmark(Request $request)
    {
        $input = $request->all();
        
        $rules=[];                      
                   
        $validator = Validator::make($input,$rules); 
        $errors = $validator->errors();
        if($errors->any()){ 
            $error_msgs = $errors->all();
            return ['err_field' => $errors, 'err_msg' => $error_msgs];
        }else{
            DB::beginTransaction();
            try {      
                $benchmark_id = request('benchmark_id');    							             
                $update_data = [
                    'player_id' => request('player_id'),
                    '10_yd_sprint' => request('10_yd_sprint') !="" ? request('10_yd_sprint') : 0,
                    '40_yd_sprint' => request('40_yd_sprint') !="" ? request('40_yd_sprint') : 0,
                    '60_yd_sprint' => request('60_yd_sprint') !="" ? request('60_yd_sprint') : 0,
                    'grip_strength' => request('grip_strength') !="" ? request('grip_strength') : 0,                  
                    'broad_jump' => request('broad_jump') !="" ? request('broad_jump') : 0,
                    'vertical_jump' => request('vertical_jump') !="" ? request('vertical_jump') : 0,
                    'ss_shoulder_flex' => request('ss_shoulder_flex') !="" ? request('ss_shoulder_flex') : 0,                  
                    'pro_agility' => request('pro_agility') !="" ? request('pro_agility') : 0,
                    'throwing_velocity' => request('throwing_velocity') !="" ? request('throwing_velocity') : 0,
                    'exit_velocity' => request('exit_velocity') !="" ? request('exit_velocity') : 0,                    
                    'stand_and_reach' => request('stand_and_reach') !="" ? request('stand_and_reach') : 0,
                    'htps' => request('htps') !="" ? request('htps') : 0,                   
                    'updated_date' => Carbon::now()->setTimezone("America/Toronto")->toDateTimeString(),
                ];                           
                Benchmark::where('id',$benchmark_id)->update($update_data);
								               
                DB::commit();                
                Session::flash('success_msg', "Your benchmark is updated.");
                return "ok";
            } catch (\Exception $ex) {
                DB::rollback();
                return "ng";
            }
        } 
    }

    public function registerReTesting(Request $request)
    {
        $input = $request->all();
        
        $rules=[];                      
                   
        $validator = Validator::make($input,$rules); 
        $errors = $validator->errors();
        if($errors->any()){ 
            $error_msgs = $errors->all();
            return ['err_field' => $errors, 'err_msg' => $error_msgs];
        }else{
            DB::beginTransaction();
            try {      
                $max_test_no = DB::table('retest')->where('player_id',request('player_id')) ->max('test_no');   
                if($max_test_no == ""){
                    $max_test_no = 1;
                }else{
                    $max_test_no = $max_test_no + 1;
                } 							             
                $insert_data = [
                    'player_id' => request('player_id'),
                    'test_no' => $max_test_no,
                    '10_yd_sprint' => request('10_yd_sprint') !="" ? request('10_yd_sprint') : 0,
                    '40_yd_sprint' => request('40_yd_sprint') !="" ? request('40_yd_sprint') : 0,
                    '60_yd_sprint' => request('60_yd_sprint') !="" ? request('60_yd_sprint') : 0,
                    'grip_strength' => request('grip_strength') !="" ? request('grip_strength') : 0,                  
                    'broad_jump' => request('broad_jump') !="" ? request('broad_jump') : 0,
                    'vertical_jump' => request('vertical_jump') !="" ? request('vertical_jump') : 0,
                    'ss_shoulder_flex' => request('ss_shoulder_flex') !="" ? request('ss_shoulder_flex') : 0,                  
                    'pro_agility' => request('pro_agility') !="" ? request('pro_agility') : 0,
                    'throwing_velocity' => request('throwing_velocity') !="" ? request('throwing_velocity') : 0,
                    'exit_velocity' => request('exit_velocity') !="" ? request('exit_velocity') : 0,                    
                    'stand_and_reach' => request('stand_and_reach') !="" ? request('stand_and_reach') : 0,
                    'htps' => request('htps') !="" ? request('htps') : 0, 
                    'inserted_date' => Carbon::now()->setTimezone("America/Toronto")->toDateTimeString(),
                ];
                

                DB::table('retest')->insert($insert_data);
								               
                DB::commit();                
                Session::flash('success_msg', "Your Re-Test is updated.");

                //send mail to admin   
                $login_id = Session::get('playerlogin')['login_id'] ; 
                $playerInfo = Player::where('id',$login_id)->first();                     
                $email_subject = "Notification";
                $sender_name = $playerInfo->first_name.' '.$playerInfo->last_name;
                $fromMail = 'ace@gmail.com';
                $ccMail = 'likhon.colgisbd@gmail.com';
                $toMail = 'ideakeek@gmail.com';          
                $email_content = $playerInfo->first_name.' '.$playerInfo->last_name.' just submitted Re-Test '.$max_test_no.'<br>';                                                     
                
                Mail::send(new SendMail($sender_name,$toMail,$fromMail,$email_subject,$email_content,$ccMail));
                if (count(Mail::failures()) > 0) {
                    DB::rollback();
                    return "no_mail";
                }else{
                    return "ok";
                }
                //send mail to admin ends

                return "ok";
            } catch (\Exception $ex) {
                DB::rollback();
                return "ng";
            }
        } 
    }
	
    public function registerPlayer(Request $request)
    {
		$file = $request->file('profile_pic');
        $request = $request->all();
        
        $rules=[];
        $rules['user_name'] = ['required','max:100','unique:player,user_name'];
        $rules['email'] = ['required','email','unique:player,email'];
		$rules['password'] = ['required','min:5','max:20'];
        $rules['con_password'] = ['nullable','required_with:password','same:password','min:5','max:20'];  
		$rules['first_name'] = ['required','max:50'];
		$rules['last_name'] = ['required','max:50'];
        $rules['address'] = ['required','max:255'];
        $rules['day'] = ['required'];	
        $rules['month'] = ['required'];	
        $rules['year'] = ['required'];	
		$rules['height'] = ['nullable','max:50'];		
		$rules['weight'] = ['nullable','max:50'];	
        $rules['weight_unit'] = ['nullable','required_with:weight'];	
        $rules['gender'] = ['required'];
        $rules['phone'] = ['required','min:10','max:11'];		
		$rules['graduation_year'] = ['nullable','max:50'];		
        $rules['profile_pic'] = ['nullable'];                         
                   
        $validator = Validator::make($request,$rules); 
        $errors = $validator->errors();
        if($errors->any()){ 
            $error_msgs = $errors->all();
            return ['err_field' => $errors, 'err_msg' => $error_msgs];
        }else{
            DB::beginTransaction();
            try {          
				if (!file_exists('uploads/player')) {
					mkdir('uploads/player', 0777, true);
				}
						
			    
				if($file != ""){
					$filename= $file->getClientOriginalName();
                    $file->move(public_path('uploads/player'),$filename);
                }else{
					$filename = "";
				}
				
				$player = new Player;
				$player->user_name = request('user_name');
				$player->first_name = request('first_name');
				$player->last_name = request('last_name');
                $player->address = request('address');
				$player->email = request('email');
                $player->phone = request('phone');
                $player->gender = request('gender');
				$player->birthday = request('year').'-'.request('month').'-'.request('day');
				$player->sport = request('sport');
				$player->height = request('height');
				$player->weight = request('weight').'_'.request('weight_unit');
				$player->level = request('level');
				$player->graduation_year = request('graduation_year');		
                $player->is_active = 0;		
				$player->filename = $filename;				
				$player->password = md5(request('password'));   
                $player->inserted_date = Carbon::now()->setTimezone("America/Toronto")->toDateTimeString();               
                $player->save();     
								               
                DB::commit();                
                Session::flash('success_msg', "You have successfully registered. Please check your inbox for an email from ace.benchmarktesting@gmail.com and confirm your account by clicking the activation link. <br /> *Check Spam/Junk folder if you do not see in your inbox.");

                //send mail starts               
                $email_subject = "User Verification";
                $sender_name = request('first_name').' '.request('last_name');
                $fromMail = 'ace@gmail.com';
                $toMail = request('email');   
                $url = url('/emailConfirmation').'/'.encrypt($toMail);  
                $email_content = "Name: ".request('first_name').' '.request('last_name').'<br>';
                $email_content .= "Password: ".request('password').'<br><br>';
                $email_content .= "Account Confirmation URL: ".$url;                                           
                
                Mail::send(new SendMail($sender_name,$toMail,$fromMail,$email_subject,$email_content));
                if (count(Mail::failures()) > 0) {
                    DB::rollback();
                    return "no_mail";
                }else{
                    Session::flash("success_mail","We Send a mail to activate your account.");
                    //return "ok";
                }
                //send mail ends

                //send mail to admin              
                $email_subject = "Notification";
                $sender_name = request('first_name').' '.request('last_name');
                $fromMail = 'ace@gmail.com';
                $ccMail = 'likhon.colgisbd@gmail.com';
                $toMail = 'ideakeek@gmail.com'; 
                $email_content = "A new player just subscribed to the ACE.".'<br>';           
                $email_content .= "Name: ".request('first_name').' '.request('last_name').'<br>';
                $email_content .= "Email: ".request('email').'<br>';
                $email_content .= "Contact number: ".request('phone');                                           
                
                Mail::send(new SendMail($sender_name,$toMail,$fromMail,$email_subject,$email_content,$ccMail));
                if (count(Mail::failures()) > 0) {
                    DB::rollback();
                    return "no_mail";
                }else{
                    return "ok";
                }
                //send mail to admin ends

                return "ok";
            } catch (\Exception $ex) {
                DB::rollback();
                return "ng";
            }
        } 
    }

    public function emailConfirmation(Request $request,$email){
        $email = decrypt($email);
        $playerInfo = DB::select("select * from player where email = '$email' ");
        if(count($playerInfo) > 0){
            $player_update_data = [
                'is_active' => 1,                                         
                'updated_date' => Carbon::now()->setTimezone("America/Toronto")->toDateTimeString(),                 
            ];
            Player::where('email',$email)->update($player_update_data);

            Session::flash("email_confirmation","Your account successfully verified.");
            return redirect('/');
            //return view('UserPanel/login');
        }else{
            Session::flash("email_confirmation","Your email is not correct.");
            return redirect('/');
            //return view('UserPanel/login');
        }
    }

    public function passwordResetEmail(Request $request){
        $input = $request->all();
        $email = $input['email'];
        
        $rules=[];
        $rules['email'] = ['required','email'];

        $validator = Validator::make($input,$rules); 
        $errors = $validator->errors();
        if($errors->any()){ 
            $error_msgs = $errors->all();
            return ['err_field' => $errors, 'err_msg' => $error_msgs];
        }
        
        $playerInfo = DB::select("select * from player where email = '$email' ");
        if(count($playerInfo) > 0){
            //send mail starts               
            $email_subject = "Password Reset";
            $sender_name = $playerInfo[0]->first_name.' '.$playerInfo[0]->last_name;
            $fromMail = 'ace@gmail.com';
            $toMail = $email; 
            $url = url('/passwordReset').'/'.encrypt($email); 
            $email_content = "Name: ".$playerInfo[0]->first_name.' '.$playerInfo[0]->last_name.'<br><br>';
            $email_content .= "Password Reset URL: ".$url;                             
            
            Mail::send(new SendMail($sender_name,$toMail,$fromMail,$email_subject,$email_content));
            if (count(Mail::failures()) > 0) {
                return "ng";
            }else{
                Session::flash("email_confirmation","We sent a mail to reset your password.");
                return "ok";
            }
            //send mail ends
        }else{
            return "no_data_found";
        }

    }

    public function passwordReset(Request $request,$email){
        $email = decrypt($email);
        $playerInfo = DB::select("select * from player where email = '$email' ");
        return view('UserPanel/passwordReset',compact('playerInfo'));
    }

    public function passwordResetPost(Request $request){
        $input = $request->all();
        $player_id = $input['player_id'];

        $rules=[];
        $rules['password'] = ['required','min:5','max:20'];
        $rules['con_password'] = ['nullable','required_with:password','same:password','min:5','max:20']; 

        $validator = Validator::make($input,$rules); 
        $errors = $validator->errors();
        if($errors->any()){ 
            $error_msgs = $errors->all();
            return ['err_field' => $errors, 'err_msg' => $error_msgs];
        }
      
        DB::beginTransaction();
        try {  
            $player_update_data = [
                'password' => md5(request('password')),                             
                'updated_date' => Carbon::now()->setTimezone("America/Toronto")->toDateTimeString(),                 
            ]; 
            Player::where('id',$player_id)->update($player_update_data);
            DB::commit();                
            Session::flash('success_msg', "Successfully Updatted You password");
            return "ok";
        } catch (\Exception $ex) {
            DB::rollback();
            return "ng";
        }
    }

    public function editProfile(){
        if(Session::has('playerlogin')){
            $player_id = Session::get('playerlogin')['login_id'] ;
            $playerInfo = DB::select("select *,SUBSTRING_INDEX(player.weight,'_',1) as weight,SUBSTRING_INDEX(player.weight,'_',-1) as weight_unit  from player where id = '$player_id'");
            $editData = $playerInfo;
            return view('UserPanel.register',compact('player_id','playerInfo','editData'));
        }else{
			return redirect('/');
		}
    }

    public function editPlayer(Request $request)
    {
		$file = $request->file('profile_pic');
        $request = $request->all();
        $player_id = request('player_id');
        
        $rules=[];
        $rules['user_name'] = ['required','max:100'];
        $rules['email'] = ['required','email'];
		$rules['password'] = ['nullable','min:5','max:20'];
        $rules['con_password'] = ['nullable','required_with:password','same:password','min:5','max:20'];  
		$rules['first_name'] = ['required','max:50'];
		$rules['last_name'] = ['required','max:50'];
        $rules['address'] = ['required','max:255'];
        $rules['day'] = ['required'];	
        $rules['month'] = ['required'];	
        $rules['year'] = ['required'];	
		$rules['height'] = ['nullable','max:50'];		
		$rules['weight'] = ['nullable','max:50'];	
        $rules['weight_unit'] = ['nullable','required_with:weight'];	
        $rules['gender'] = ['required'];
        $rules['phone'] = ['required','max:11'];		
		$rules['graduation_year'] = ['nullable','max:50'];	
        if(request('old_filename') == ""){	
           $rules['profile_pic'] = ['nullable'];   
        }                      
                   
        $validator = Validator::make($request,$rules); 
        $errors = $validator->errors();
        if($errors->any()){ 
            $error_msgs = $errors->all();
            return ['err_field' => $errors, 'err_msg' => $error_msgs];
        }else{
            DB::beginTransaction();
            try {          
				if (!file_exists('uploads/player')) {
					mkdir('uploads/player', 0777, true);
				}
						
			    
				if($file != ""){
					$filename= $file->getClientOriginalName();
                    $file->move(public_path('uploads/player'),$filename);
                }else{
					$filename = request('old_filename');
				}
								  
                
                if(request('password') != ""){
                    $player_update_data = [
                        'user_name' => request('user_name'),
                        'first_name' => request('first_name'),
                        'last_name' => request('last_name'),   
                        'address' => request('address'),             
                        'email' => request('email'),                                       
                        'phone' => request('phone'),
                        'gender' => request('gender'),
                        'birthday' => request('year').'-'.request('month').'-'.request('day'),
                        'sport' => request('sport'),
                        'height' => request('height'),
                        'weight' => request('weight').'_'.request('weight_unit'),
                        'level' => request('level'),
                        'graduation_year' => request('graduation_year'),
                        'filename' => $filename,
                        'password' => md5(request('password')),
                        'updated_date' => Carbon::now()->setTimezone("America/Toronto")->toDateTimeString(),                   
                    ];
                }else{
                    $player_update_data = [
                        'user_name' => request('user_name'),
                        'first_name' => request('first_name'),
                        'last_name' => request('last_name'),   
                        'address' => request('address'),              
                        'email' => request('email'),                                       
                        'phone' => request('phone'),
                        'gender' => request('gender'),
                        'birthday' => request('year').'-'.request('month').'-'.request('day'),
                        'sport' => request('sport'),
                        'height' => request('height'),
                        'weight' => request('weight').'_'.request('weight_unit'),
                        'level' => request('level'),
                        'graduation_year' => request('graduation_year'),
                        'filename' => $filename,                      
                        'updated_date' => Carbon::now()->setTimezone("America/Toronto")->toDateTimeString(),                 
                    ];
                } 
                Player::where('id',$player_id)->update($player_update_data);
								               
                DB::commit();                
                Session::flash('success_msg', "Your profile has been updated.");
                return "ok";
            } catch (\Exception $ex) {
                DB::rollback();
                return "ng";
            }
        } 
    }
	
	public function login(Request $request)
    {
		$request = $request->all();
		
        $rules=[];
        $rules['email'] = ['required'];
        $rules['password'] = ['required'];
       
        $validator = Validator::make($request,$rules); 
        $errors = $validator->errors();
        if($errors->any()){ 
            $error_msgs = $errors->all();
            return ['err_field' => $errors, 'err_msg' => $error_msgs];
        }else{
            $email = request('email');
            $password = md5(request('password'));
            //$loginInfo = DB::select("select * from player where email = '$email' and password = '$password' and is_active = 1");
            $loginInfo = DB::select("select * from player where email = '$email' and password = '$password'");
            if(count($loginInfo) > 0){
                if($loginInfo[0]->is_active == 0){
                    $result['status'] = "inactive";
                    return $result;
                }else{
                    $id = $loginInfo[0]->id;
                    $login_session = [
                            'login_id' => $id,
                            'login_name' => $loginInfo[0]->user_name,
                            'login_email' => $loginInfo[0]->email,
                        ];
                    session()->put('playerlogin', $login_session);               
                    $result['status'] = "ok";
                    return $result;
                }              

            }else{
                $result['status'] = "ng";
                return $result;
            }
        }
    }
    
}
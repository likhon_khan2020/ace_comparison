<?php

namespace App\Http\Controllers\AdminPanel;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Session;
use Illuminate\Http\Request;
use File;
use Mail;
use App\Mail\SendMail;
Use \Carbon\Carbon;
use App\Model\Admin;
use App\Model\Player;
use App\Model\Benchmark;
use App\Model\Retest;
use App\Model\Goal;
use DB;

class AdminController extends Controller
{
    public function index(){
        if(Session::has('adminlogin')){
            return redirect('/adminProfile');
        }else{
            return view('AdminPanel/login');
        }
    }

    public function addAdmin(){
        if(Session::has('adminlogin')){
            $login_id = Session::get('adminlogin')['admin_login_id'] ;
            $adminInfo = DB::select("select * from admin where id = '$login_id'");
            return view('AdminPanel.addAdmin',compact('login_id','adminInfo'));
        }else{
			return redirect('/admin');
		}
    }

    public function register(){
        if(Session::has('adminlogin')){   
			$login_id = Session::get('adminlogin')['admin_login_id'] ;
			$adminInfo = DB::select("select * from admin where id = '$login_id'");
		    return view('AdminPanel.register',compact('adminInfo'));
        }else{
            return view('AdminPanel/login');
        }
	}

    public function registerPlayer(Request $request)
    {
		$file = $request->file('profile_pic');
        $request = $request->all();
        
        $rules=[];
        $rules['user_name'] = ['required','max:100'];
        $rules['email'] = ['required','email','unique:player,email'];
		$rules['password'] = ['required','min:5','max:20'];
        $rules['con_password'] = ['nullable','required_with:password','same:password','min:5','max:20'];  
		$rules['first_name'] = ['required','max:50'];
		$rules['last_name'] = ['required','max:50'];
        $rules['address'] = ['required','max:255'];
        $rules['day'] = ['required'];	
        $rules['month'] = ['required'];	
        $rules['year'] = ['required'];	
		$rules['height'] = ['nullable','max:50'];		
		$rules['weight'] = ['nullable','max:50'];	
        $rules['weight_unit'] = ['nullable','required_with:weight'];	
        $rules['gender'] = ['required'];
        $rules['phone'] = ['required','min:10','max:11'];		
		$rules['graduation_year'] = ['nullable','max:50'];		
        $rules['profile_pic'] = ['nullable'];                         
                   
        $validator = Validator::make($request,$rules); 
        $errors = $validator->errors();
        if($errors->any()){ 
            $error_msgs = $errors->all();
            return ['err_field' => $errors, 'err_msg' => $error_msgs];
        }else{
            DB::beginTransaction();
            try {          
				if (!file_exists('uploads/player')) {
					mkdir('uploads/player', 0777, true);
				}
						
			    
				if($file != ""){
					$filename= $file->getClientOriginalName();
                    $file->move(public_path('uploads/player'),$filename);
                }else{
					$filename = "";
				}
				
				$player = new Player;
				$player->user_name = request('user_name');
				$player->first_name = request('first_name');
				$player->last_name = request('last_name');
                $player->address = request('address');
				$player->email = request('email');
                $player->phone = request('phone');
                $player->gender = request('gender');
				$player->birthday = request('year').'-'.request('month').'-'.request('day');
				$player->sport = request('sport');
				$player->height = request('height');
				$player->weight = request('weight').request('weight_unit');
				$player->level = request('level');
				$player->graduation_year = request('graduation_year');				
				$player->filename = $filename;				
				$player->password = md5(request('password'));   
                $player->inserted_date = Carbon::now()->setTimezone("America/Toronto")->toDateTimeString();               
                $player->save();     
								               
                DB::commit();                
                Session::flash('success_msg', "A new has been registered successfully, please let the admin know to check the inbox (email) for verifying the account using the Account confirmation URL.");

                 //send mail starts               
                 $email_subject = "User Verification";
                 $sender_name = request('first_name').' '.request('last_name');
                 $fromMail = 'ace@gmail.com';
                 $toMail = request('email');   
                 $url = url('/PlayerEmailConfirmation').'/'.encrypt($toMail);  
                 $email_content = "Name: ".request('first_name').' '.request('last_name').'<br>';
                 $email_content .= "Password: ".request('password').'<br><br>';
                 $email_content .= "Account Confirmation URL: ".$url;                                           
                 
                 Mail::send(new SendMail($sender_name,$toMail,$fromMail,$email_subject,$email_content));
                 if (count(Mail::failures()) > 0) {
                     DB::rollback();
                     return "no_mail";
                 }else{
                     Session::flash("success_mail","We Send a mail to activate your account.");
                     return "ok";
                 }
                 //send mail ends

                return "ok";
            } catch (\Exception $ex) {
                DB::rollback();
                return "ng";
            }
        } 
    }


    public function PlayerEmailConfirmation(Request $request,$email){
        $email = decrypt($email);
        $playerInfo = DB::select("select * from player where email = '$email' ");
        if(count($playerInfo) > 0){
            $player_update_data = [
                'is_active' => 1,                                         
                'updated_date' => Carbon::now()->setTimezone("America/Toronto")->toDateTimeString(),                 
            ];
            Player::where('email',$email)->update($player_update_data);

            Session::flash("email_confirmation","Your Account Successfully activated.");
            return redirect('/');
            //return view('UserPanel/login');
        }else{
            Session::flash("email_confirmation","Your email is not correct.");
            return redirect('/');
            //return view('UserPanel/login');
        }
    }

    public function compareRetest(Request $request){
        $input = $request->all();
        $retest_id = $input['retest_id'];
        $level_id = $input['level'];
        $player_id = $input['player_id'];
        if(Session::has('adminlogin')){
            $login_id = Session::get('adminlogin')['admin_login_id'] ;
            $adminInfo = DB::select("select * from admin where id = '$login_id'");
            $benchmarkInfo = Benchmark::where('player_id',$player_id)->first()->toArray();
            $proflBenchmarkInfo = DB::table('benchmark')
                                ->select('benchmark.*','player.level as level_id','level.level_name' )
                                ->leftJoin('player', 'player.id', '=', 'benchmark.player_id')
                                ->leftJoin('level', 'level.id', '=', 'player.level')
                                ->where('player_id',$player_id)
                                ->get();
            $retestInfo = Retest::select('retest.*','player.first_name','player.last_name','level.level_name')->leftJoin('player','player.id','=','retest.player_id')->leftJoin('level','level.id','=','player.level')->where('retest.id',$retest_id)->first();  
            //dd($retestInfo);
            if(!$retestInfo){
                $retestInfo['first_name'] = "";
                $retestInfo['last_name'] = "";
                $retestInfo['level_name'] = "";
                $retestInfo['10_yd_sprint'] = 0;
                $retestInfo['40_yd_sprint'] = 0;
                $retestInfo['60_yd_sprint'] = 0;
                $retestInfo['grip_strength'] = 0;
                $retestInfo['broad_jump'] = 0;
                $retestInfo['vertical_jump'] = 0;
                $retestInfo['ss_shoulder_flex'] = 0;
                $retestInfo['pro_agility'] = 0;
                $retestInfo['throwing_velocity'] = 0;
                $retestInfo['exit_velocity'] = 0;
                $retestInfo['stand_and_reach'] = 0;
                $retestInfo['htps'] = 0;
                $retestInfo['inserted_date'] = "";
            } 
            $goalInfo = Goal::where('level_id',$level_id)->first();              
			if(count($adminInfo) > 0){
                if(!is_array($retestInfo)){
                    $retestInfo = $retestInfo->toArray();
                }
                if($goalInfo){
                    $goalInfo = $goalInfo->toArray();
                }else{
                    $goalInfo  = [];
                }
                
				return view('AdminPanel.compareRetest',compact('adminInfo','benchmarkInfo','proflBenchmarkInfo','retestInfo','goalInfo'));
			}else{
				return redirect('/admin');
			}
		}else{
			return redirect('/admin');
		}
    }

    public function registerAdmin(Request $request)
    {
		$file = $request->file('profile_pic');
        $request = $request->all();
        
        $rules=[];
        $rules['user_name'] = ['required','max:100'];
        $rules['email'] = ['required','email','unique:admin,email'];
		$rules['password'] = ['required','min:5','max:20'];
        $rules['con_password'] = ['nullable','required_with:password','same:password','min:5','max:20'];  
		$rules['first_name'] = ['required','max:50'];
		$rules['last_name'] = ['required','max:50'];
        $rules['day'] = ['required'];	
        $rules['month'] = ['required'];	
        $rules['year'] = ['required'];	
        $rules['phone'] = ['required','max:11'];		
        $rules['profile_pic'] = ['nullable'];                         
                   
        $validator = Validator::make($request,$rules); 
        $errors = $validator->errors();
        if($errors->any()){ 
            $error_msgs = $errors->all();
            return ['err_field' => $errors, 'err_msg' => $error_msgs];
        }else{
            DB::beginTransaction();
            try {          
				if (!file_exists('uploads/admin')) {
					mkdir('uploads/admin', 0777, true);
				}	
			    
				if($file != ""){
					$filename= $file->getClientOriginalName();
                    $file->move(public_path('uploads/admin'),$filename);
                }else{
					$filename = "";
				}
				
				$admin = new Admin;
				$admin->user_name = request('user_name');
				$admin->first_name = request('first_name');
				$admin->last_name = request('last_name');
				$admin->email = request('email');
                $admin->phone = request('phone');
				$admin->dob = request('year').'-'.request('month').'-'.request('day');		
				$admin->filename = $filename;				
				$admin->password = md5(request('password'));   
                $admin->inserted_date = Carbon::now()->setTimezone("America/Toronto")->toDateTimeString(); 
                $admin->inserted_by = request('inserted_by');              
                $admin->save();     
								               
                DB::commit();                
                Session::flash('success_msg', "You have successfully registered. Please check your inbox for an email from ace.benchmarktesting@gmail.com and confirm your account by clicking the activation link. <br /> *Check Spam/Junk folder if you do not see in your inbox.");

                //send mail starts               
                $email_subject = "User Verification";
                $sender_name = request('first_name').' '.request('last_name');
                $fromMail = 'ace@gmail.com';
                $toMail = request('email');   
                $url = url('/adminEmailConfirmation').'/'.encrypt($toMail);  
                //$email_content = "Name: ".request('first_name').' '.request('last_name').'<br>';
                $email_content = "Email: ".request('email').'<br>';
                $email_content .= "Password: ".request('password').'<br><br>';
                $email_content .= "Account Confirmation URL: ".$url;                                           
                
                Mail::send(new SendMail($sender_name,$toMail,$fromMail,$email_subject,$email_content));
                if (count(Mail::failures()) > 0) {
                    DB::rollback();
                    return "no_mail";
                }else{
                    Session::flash("success_mail","We Send a mail to activate your account.");
                    return "ok";
                }
                //send mail ends

                return "ok";
            } catch (\Exception $ex) {
                DB::rollback();
                return "ng";
            }
        } 
    }

    public function emailConfirmation(Request $request,$email){
        $email = decrypt($email);
        $adminInfo = DB::select("select * from admin where email = '$email' ");
        if(count($adminInfo) > 0){
            $admin_update_data = [
                'is_active' => 1,                                         
                'updated_date' => Carbon::now()->setTimezone("America/Toronto")->toDateTimeString(),                 
            ];
            Admin::where('email',$email)->update($admin_update_data);

            Session::flash("email_confirmation","Your Account Successfully activated.");
            return redirect('/admin');      
        }else{
            Session::flash("email_confirmation","Your email is not correct.");
            return redirect('/admin');
        }
    }


    public function editAdminProfile(){
        if(Session::has('adminlogin')){
            $login_id = Session::get('adminlogin')['admin_login_id'] ;
            $adminInfo = DB::select("select * from admin where id = '$login_id'");
            $editData = $adminInfo;
            return view('AdminPanel.addAdmin',compact('login_id','adminInfo','editData'));
        }else{
			return redirect('/admin');
		}
    }

    public function editAdmin(Request $request)
    {
		$file = $request->file('profile_pic');
        $request = $request->all();
        $admin_id = request('admin_id');
        
        $rules=[];
        $rules['user_name'] = ['required','max:100'];
        $rules['email'] = ['required','email'];    
		$rules['password'] = ['nullable','min:5','max:20'];
        $rules['con_password'] = ['nullable','required_with:password','same:password','min:5','max:20'];  
		$rules['first_name'] = ['required','max:50'];
		$rules['last_name'] = ['required','max:50'];
        $rules['day'] = ['required'];	
        $rules['month'] = ['required'];	
        $rules['year'] = ['required'];	
        $rules['phone'] = ['required','max:11'];	
        if(request('old_filename') == ""){
            $rules['profile_pic'] = ['required'];  
        }	                               
                   
        $validator = Validator::make($request,$rules); 
        $errors = $validator->errors();
        if($errors->any()){ 
            $error_msgs = $errors->all();
            return ['err_field' => $errors, 'err_msg' => $error_msgs];
        }else{
            DB::beginTransaction();
            try {          
				if (!file_exists('uploads/admin')) {
					mkdir('uploads/admin', 0777, true);
				}	
			    
				if($file != ""){
					$filename= $file->getClientOriginalName();
                    $file->move(public_path('uploads/admin'),$filename);
                }else{
					$filename = request('old_filename');
				}
            	
                if(request('password') != ""){
                    $admin_update_data = [
                        'user_name' => request('user_name'),
                        'first_name' => request('first_name'),
                        'last_name' => request('last_name'),              
                        'email' => request('email'),                                       
                        'phone' => request('phone'),
                        'dob' => request('year').'-'.request('month').'-'.request('day'),
                        'filename' => $filename,
                        'password' => md5(request('password')),
                        'updated_date' => Carbon::now()->setTimezone("America/Toronto")->toDateTimeString(),                   
                    ];
                }else{
                    $admin_update_data = [
                        'user_name' => request('user_name'),
                        'first_name' => request('first_name'),
                        'last_name' => request('last_name'),              
                        'email' => request('email'),                                       
                        'phone' => request('phone'),
                        'dob' => request('year').'-'.request('month').'-'.request('day'),
                        'filename' => $filename,
                        'updated_date' => Carbon::now()->setTimezone("America/Toronto")->toDateTimeString(),                   
                    ];
                }              
                Admin::where('id',$admin_id)->update($admin_update_data);
								               
                DB::commit();                
                Session::flash('success_msg', "Your profile has been updated.");
                return "ok";
            } catch (\Exception $ex) {
                DB::rollback();
                return "ng";
            }
        } 
    }

    public function passwordResetEmail(Request $request){
        $input = $request->all();
        $email = $input['email'];
        
        $rules=[];
        $rules['email'] = ['required','email'];

        $validator = Validator::make($input,$rules); 
        $errors = $validator->errors();
        if($errors->any()){ 
            $error_msgs = $errors->all();
            return ['err_field' => $errors, 'err_msg' => $error_msgs];
        }
        
        $adminInfo = DB::select("select * from admin where email = '$email' ");
        if(count($adminInfo) > 0){
            //send mail starts               
            $email_subject = "Password Reset";
            $sender_name = $adminInfo[0]->first_name.' '.$adminInfo[0]->last_name;
            $fromMail = 'ace@gmail.com';
            $toMail = $email; 
            $url = url('/admin/passwordReset').'/'.encrypt($email); 
            $email_content = "Name: ".$adminInfo[0]->first_name.' '.$adminInfo[0]->last_name.'<br><br>';
            $email_content .= "Password Reset URL: ".$url;                             
            
            Mail::send(new SendMail($sender_name,$toMail,$fromMail,$email_subject,$email_content));
            if (count(Mail::failures()) > 0) {
                return "ng";
            }else{
                Session::flash("email_confirmation","We sent a mail to reset your password.");
                return "ok";
            }
            //send mail ends
        }else{
            return "no_data_found";
        }

    }

    public function passwordReset(Request $request,$email){
        $email = decrypt($email);
        $adminInfo = DB::select("select * from admin where email = '$email' ");
        return view('AdminPanel/passwordReset',compact('adminInfo'));
    }

    public function passwordResetPost(Request $request){
        $input = $request->all();
        $admin_id = $input['admin_id'];

        $rules=[];
        $rules['password'] = ['required','min:5','max:20'];
        $rules['con_password'] = ['nullable','required_with:password','same:password','min:5','max:20']; 

        $validator = Validator::make($input,$rules); 
        $errors = $validator->errors();
        if($errors->any()){ 
            $error_msgs = $errors->all();
            return ['err_field' => $errors, 'err_msg' => $error_msgs];
        }
      
        DB::beginTransaction();
        try {  
            $admin_update_data = [
                'password' => md5(request('password')),                             
                'updated_date' => Carbon::now()->setTimezone("America/Toronto")->toDateTimeString(),                 
            ]; 
            Admin::where('id',$admin_id)->update($admin_update_data);
            DB::commit();                
            Session::flash('success_msg', "Successfully Updatted You password");
            return "ok";
        } catch (\Exception $ex) {
            DB::rollback();
            return "ng";
        }
    }

    public function viewAdmin(){
        if(Session::has('adminlogin')){
			$login_id = Session::get('adminlogin')['admin_login_id'] ;
            $adminInfo = DB::select("select * from admin where id = '$login_id'");
			$adminList = DB::select("select * from admin");     
            $query = DB::table('admin')->select('admin.*' )->where('id','!=',$login_id)->orderBy('admin.id', 'DESC');
            $adminList = $query->paginate(10);     
			if(count($adminList) > 0){
				return view('AdminPanel.viewAdmin',compact('adminInfo','adminList'));
			}else{
				return redirect('/admin');
			}
		}else{
			return redirect('/admin');
		}
    }

    public function viewPlayer(){
        if(Session::has('adminlogin')){
			$login_id = Session::get('adminlogin')['admin_login_id'] ;
            $adminInfo = DB::select("select * from admin where id = '$login_id'");
			$adminList = DB::select("select * from admin");     
            $query = DB::table('player')->select('player.*' )->orderBy('player.id', 'DESC');
            $playerList = $query->paginate(10);     
			if(count($playerList) > 0){
				return view('AdminPanel.viewPlayer',compact('adminInfo','playerList'));
			}else{
				return redirect('/admin');
			}
		}else{
			return redirect('/admin');
		}
    }

    public function enableDisableAdmin(Request $request)
    {
        $request = $request->all(); 
        $admin_id = $request['admin_id'];       
            
        DB::beginTransaction();
        try {      
            if($request['type'] == 'inactive'){
                $is_active = 0;
            } else{
                $is_active = 1;
            }  

            $admin_update_data = [
                'is_active' => $is_active,                          
            ];             
            Admin::where('id',$admin_id)->update($admin_update_data);
                                            
            DB::commit();                
            return "ok";
        } catch (\Exception $ex) {
            DB::rollback();
            return "ng";
        }
        
    }

    public function enableDisablePlayer(Request $request)
    {
        $request = $request->all(); 
        $player_id = $request['player_id'];       
            
        DB::beginTransaction();
        try {      
            if($request['type'] == 'inactive'){
                $is_active = 0;
                $activated_msg = "Your account deactivated.";
            } else{
                $is_active = 1;
                $activated_msg = "Your account activated.";
            }  

            $player_update_data = [
                'is_active' => $is_active,                          
            ];             
            Player::where('id',$player_id)->update($player_update_data);

            $playerInfo = DB::select("select * from player where id = '$player_id'");

            //send mail starts               
            $email_subject = "User Verification";
            $sender_name = $playerInfo[0]->first_name.' '.$playerInfo[0]->last_name;
            $fromMail = 'ace@gmail.com';
            $toMail = $playerInfo[0]->email;              
            $email_content = $playerInfo[0]->first_name.' '.$playerInfo[0]->last_name.','.$activated_msg.'<br>';
            //$email_content .= "Password: ".request('password').'<br>';
            $email_content .= "Email: ".$playerInfo[0]->email;                                           
            
            Mail::send(new SendMail($sender_name,$toMail,$fromMail,$email_subject,$email_content));
            //if (count(Mail::failures()) > 0) {
            //    DB::rollback();
            //    return "no_mail";
            //}else{
            //    Session::flash("success_mail","We Send a mail.");
            //    return "ok";
            //}
            //send mail ends
                                            
            DB::commit();                
            return "ok";
        } catch (\Exception $ex) {
            DB::rollback();
            return "ng";
        }
        
    }

    public function adminProfile(Request $request){
        if(Session::has('adminlogin')){
            $request = $request->all();        
			$login_id = Session::get('adminlogin')['admin_login_id'] ;
			$adminInfo = DB::select("select * from admin where id = '$login_id'");
            $playerList = DB::select("select * from player");
            $proflBenchmarkInfo = DB::select("select * from benchmark where player_id = '$login_id'");

            DB::select("DROP TABLE IF EXISTS test_temp");
            DB::select(
            "CREATE TEMPORARY TABLE test_temp as
                select 
                test_table.*,
                CASE
                    WHEN (select count(id) from retest where player_id = test_table.player_id) > 0 THEN 'retest'  
                    ELSE  'benchmark' END as test_status   
                from (
                    (select * from retest)
                    UNION
                    (select * from benchmark where (select count(id) from retest where player_id = benchmark.player_id) = 0)
                ) as test_table     
            ");
            //$data = DB::table('test_temp')->get();    
            //dd($data);      

            $query = DB::table('test_temp')->select('test_temp.*',DB::raw("CONCAT(player.first_name,' ',player.last_name) AS player_name") )
                    ->leftJoin('player', 'player.id', '=', 'test_temp.player_id');
                if(isset($request['player_id']) && $request['player_id'] != ""){
                    $query->where('player_id',$request['player_id']);
                }
                if(isset($request['start_date']) && isset($request['end_date']) && $request['start_date'] != "" && $request['end_date'] != ""){
                    $start_date = $request['start_date'].' 00:00:00';
                    $end_date = $request['end_date'].' 23:59:59';
                    //$query->where(DB::raw("retest.inserted_date >= $start_date   AND  retest.inserted_date  <= $end_date"));
                    $query->whereBetween('test_temp.inserted_date', [$start_date, $end_date]);
                    
                }
                
            $retestList = $query->paginate(10);
			if(count($adminInfo) > 0){
				return view('AdminPanel.adminProfile',compact('adminInfo','proflBenchmarkInfo','retestList','playerList'));
			}else{
				return redirect('/admin');
			}
		}else{
			return redirect('/admin');
		}
    }
   
	public function login(Request $request)
    {
		$request = $request->all();
		
        $rules=[];
        $rules['email'] = ['required'];
        $rules['password'] = ['required'];
       
        $validator = Validator::make($request,$rules); 
        $errors = $validator->errors();
        if($errors->any()){ 
            $error_msgs = $errors->all();
            return ['err_field' => $errors, 'err_msg' => $error_msgs];
        }else{
            $email = request('email');
            $password = md5(request('password'));
            $loginInfo = DB::select("select * from admin where email = '$email' and password = '$password' and is_active = 1");
            if(count($loginInfo) > 0){
                $id = $loginInfo[0]->id;
                $login_session = [
                        'admin_login_id' => $id,
                        'admin_login_name' => $loginInfo[0]->user_name,
                        'admin_login_email' => $loginInfo[0]->email,
                    ];
                session()->put('adminlogin', $login_session);               
                $result['status'] = "ok";
                return $result;
            }else{
                $result['status'] = "ng";
                return $result;
            }
        }
    }
	
	public function logout()
    {
        session()->forget('adminlogin');
        return redirect('/admin');
    }
    
}
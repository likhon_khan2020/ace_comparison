<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Retest extends Model
{
    protected $table = 'retest';
    protected $primaryKey = 'id';
    public $timestamps = false;
}

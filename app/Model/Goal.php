<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Goal extends Model
{
    protected $table = 'goal';
    protected $primaryKey = 'id';
    public $timestamps = false;
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Benchmark extends Model
{
    protected $table = 'benchmark';
    protected $primaryKey = 'id';
    public $timestamps = false;
}

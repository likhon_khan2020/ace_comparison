<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sender_name,$toMail,$fromMail,$email_subject,$email_content,$ccMail = null)
    {
        $this->name = $sender_name;
        $this->toMail = $toMail;
        $this->fromMail = $fromMail;       
        $this->subject = $email_subject;
        $this->content = $email_content;
        $this->ccMail = $ccMail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->name;
        $toMial = $this->toMail;
        $fromMail = $this->fromMail;
        $subject = $this->subject;     
        $html = $this->content;
        $ccMail = $this->ccMail;
              
        if($ccMail == null){
            $this->html($html)->to($toMial)->from($fromMail,$name)->subject($subject);
        }else{
            $this->html($html)->to($toMial)->from($fromMail,$name)->cc($ccMail)->subject($subject);
        }
          
    }
}

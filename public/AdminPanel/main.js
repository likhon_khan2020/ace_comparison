function registerAdmin(url) {
    var data = new FormData(document.getElementById('registrationForm'));
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        contentType: false,
        cache: false,
        processData: false,
        success: function (result) {
            //console.log(result);
            if ($.trim(result) == 'ok') {
                $("#error_data").css("display","none");
                document.getElementById('title').scrollIntoView();
                location.reload();
            }else if ($.trim(result) == 'ng') {
                document.getElementById('title').scrollIntoView();
                $(".loader").css("display",'none');
                var html = "<p style='color:red;font-size: 16px;margin:0px;'>Something went wrong!</p>";
                $('#error_data').html(html);
            }else if ($.trim(result) == 'no_mail') {
                document.getElementById('title').scrollIntoView();
                $(".loader").css("display",'none');
                $("#submit").attr("disabled",false);
                var html = "<p style='color:red;font-size: 12px;margin:0px;'>Something went wrong to send mail!</p>";
                $('#error_data').html(html); 
            }else {
                $(".loader").css("display",'none');
                document.getElementById('title').scrollIntoView();                
                $("#submit").attr("disabled",false); 

                var inputError = result.err_field;
                console.log(inputError);

                var html = '';
                if (result.err_msg) {
                    html = '<div>';
                    for (var count = 0; count < result.err_msg.length; count++) {
                        html += '<p style="color:red;font-size: 12px;margin:0px;">' + result.err_msg[count] + '</p>';
                    }
                    html += '</div>';
                    $('#error_data').html(html);                        
                    $("#error_data").show();
                }
              
                if (inputError.user_name) {
                    $('#user_name').addClass("error");
                } else {
                    $('#user_name').removeClass("error");
                }

                if (inputError.email) {
                    $('#email').addClass("error");
                } else {
                    $('#email').removeClass("error");
                }
                
                if (inputError.password) {
                    $('#password').addClass("error");
                } else {
                    $('#password').removeClass("error");
                }
                
                if (inputError.con_password) {
                    $('#con_password').addClass("error");
                } else {
                    $('#con_password').removeClass("error");
                }

                if (inputError.first_name) {
                    $('#first_name').addClass("error");
                } else {
                    $('#first_name').removeClass("error");
                }
                
                if (inputError.last_name) {
                    $('#last_name').addClass("error");
                } else {
                    $('#last_name').removeClass("error");
                }

                if (inputError.day) {
                    $('#day').addClass("error");
                } else {
                    $('#day').removeClass("error");
                }
                if (inputError.month) {
                    $('#month').addClass("error");
                } else {
                    $('#month').removeClass("error");
                }

                if (inputError.year) {
                    $('#year').addClass("error");
                } else {
                    $('#year').removeClass("error");
                }             
                
                if (inputError.phone) {
                    $('#phone').addClass("error");
                } else {
                    $('#phone').removeClass("error");
                }
               
                if (inputError.profile_pic) {
                    $('#profile_pic').addClass("error");
                } else {
                    $('#profile_pic').removeClass("error");
                }

            }
        },
        beforeSend: function(){
            $(".loader").css("display",'block');
        }
    });
//}
}

function editAdmin(url) {
    var data = new FormData(document.getElementById('registrationForm'));
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        contentType: false,
        cache: false,
        processData: false,
        success: function (result) {
            //console.log(result);
            if ($.trim(result) == 'ok') {
                //location.reload();
                $("#submit").prop('disabled',true)
                document.getElementById('title').scrollIntoView();
                var html = "<div class='alert alert-success alert-dismissible' role='alert'>"+
                "<strong>Your profile has been updated.</strong>"+
                "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>"+
                  "<span aria-hidden='true'>&times;</span>"+
                "</button>"+
                "</div>";
               $("#success").html(html);
                setTimeout(function(){
                    window.location.href="adminProfile";
                },2500); 
            }else if ($.trim(result) == 'ng') {
                location.reload();
            }else {
                var inputError = result.err_field;
                console.log(inputError);

                var html = '';
                if (result.err_msg) {
                    html = '<div>';
                    for (var count = 0; count < result.err_msg.length; count++) {
                        html += '<p style="color:red;font-size: 12px;margin:0px;">' + result.err_msg[count] + '</p>';
                    }
                    html += '</div>';
                    $('#error_data').html(html);                        
                    $("#error_data").show();
                }
              
                if (inputError.user_name) {
                    $('#user_name').addClass("error");
                } else {
                    $('#user_name').removeClass("error");
                }

                if (inputError.email) {
                    $('#email').addClass("error");
                } else {
                    $('#email').removeClass("error");
                }
                
                if (inputError.password) {
                    $('#password').addClass("error");
                } else {
                    $('#password').removeClass("error");
                }
                
                if (inputError.con_password) {
                    $('#con_password').addClass("error");
                } else {
                    $('#con_password').removeClass("error");
                }

                if (inputError.first_name) {
                    $('#first_name').addClass("error");
                } else {
                    $('#first_name').removeClass("error");
                }
                
                if (inputError.last_name) {
                    $('#last_name').addClass("error");
                } else {
                    $('#last_name').removeClass("error");
                }

                if (inputError.day) {
                    $('#day').addClass("error");
                } else {
                    $('#day').removeClass("error");
                }
                if (inputError.month) {
                    $('#month').addClass("error");
                } else {
                    $('#month').removeClass("error");
                }

                if (inputError.year) {
                    $('#year').addClass("error");
                } else {
                    $('#year').removeClass("error");
                }             
                
                if (inputError.phone) {
                    $('#phone').addClass("error");
                } else {
                    $('#phone').removeClass("error");
                }
               
                if (inputError.profile_pic) {
                    $('#profile_pic').addClass("error");
                } else {
                    $('#profile_pic').removeClass("error");
                }

            }
        }
    });
//}
}

function passwordResetEmail() {
    var data = $('#passwordResetForm').serialize();
    console.log(data);
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('#password_reset_token').val()
        },
        type: 'POST',
        url: 'admin/passwordResetEmail',
        data: data,
        success: function (result) {
            //console.log(result);
            if ($.trim(result) == 'ok') {
                //var base_url = window.location.origin+window.location.pathname;
                var html = "<span style='color:green;position: relative;bottom:7px;'>We Send a mail to reset your password.</span>";
                $('#error_data').html(""); 
                $('#email').removeClass("error");
                $("#passwordResetForm").trigger("reset");
                $("#login_success").html(html);
                $("#passwordResetModal").modal('hide');
            }else {
                var inputError = result.err_field;
                console.log(inputError);

                var html = '';
                if (result.err_msg) {
                    html = '<div>';
                    for (var count = 0; count < result.err_msg.length; count++) {
                        html += '<p style="color:red;font-size: 12px;margin:0px;">' + result.err_msg[count] + '</p>';
                    }
                    html += '</div>';
                    $('#error_data').html(html);                        
                    $("#error_data").show();
                }
              
                if (inputError.email) {
                   $('#email').addClass("error");
                } else {
                   $('#email').removeClass("error");
                }

            }
        }
    });
}

function passwordReset(url) {
    var data = $('#passwordResetForm').serialize();
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function (result) {
            console.log(result);
            if ($.trim(result) == 'ok') {             
                window.location.href = base_url+"/admin";
            }else if ($.trim(result) == 'ng') {
                location.reload();
            }else {
                var inputError = result.err_field;
                console.log(inputError);

                var html = '';
                if (result.err_msg) {
                    html = '<div>';
                    for (var count = 0; count < result.err_msg.length; count++) {
                        html += '<p style="color:red;font-size: 12px;margin:0px;">' + result.err_msg[count] + '</p>';
                    }
                    html += '</div>';
                    $('#error_data').html(html);                        
                    $("#error_data").show();
                }
              
                if (inputError.password) {
                   $('#password').addClass("error");
                } else {
                   $('#password').removeClass("error");
                }

                if (inputError.con_password) {
                    $('#con_password').addClass("error");
                 } else {
                    $('#con_password').removeClass("error");
                 }

            }
        }
    });
//}
}

function registerPlayer(url) {
    var data = new FormData(document.getElementById('registrationForm'));
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        contentType: false,
        cache: false,
        processData: false,
        success: function (result) {
            //console.log(result);
            if ($.trim(result) == 'ok') {
                $("#error_data").css("display","none");
                document.getElementById('title').scrollIntoView();
                window.location.href="viewPlayer";
            }else if ($.trim(result) == 'ng') {
                document.getElementById('title').scrollIntoView();
                $(".loader").css("display",'none');
                var html = "<p style='color:red;font-size: 16px;margin:0px;'>Something went wrong!</p>";
                $('#error_data').html(html); 
            }else if ($.trim(result) == 'no_mail') {
                document.getElementById('title').scrollIntoView();
                $(".loader").css("display",'none');
                $("#submit").attr("disabled",false);
                var html = "<p style='color:red;font-size: 12px;margin:0px;'>Something went wrong to send mail!</p>";
                $('#error_data').html(html); 
            }else {
                document.getElementById('title').scrollIntoView();
                $(".loader").css("display",'none');
                $("#submit").attr("disabled",false);  

                var inputError = result.err_field;
                console.log(inputError);

                var html = '';
                if (result.err_msg) {
                    html = '<div>';
                    for (var count = 0; count < result.err_msg.length; count++) {
                        html += '<p style="color:red;font-size: 12px;margin:0px;">' + result.err_msg[count] + '</p>';
                    }
                    html += '</div>';
                    $('#error_data').html(html);                        
                    $("#error_data").show();
                }
              
                if (inputError.user_name) {
                    $('#user_name').addClass("error");
                } else {
                    $('#user_name').removeClass("error");
                }

                if (inputError.email) {
                    $('#email').addClass("error");
                } else {
                    $('#email').removeClass("error");
                }
                
                if (inputError.password) {
                    $('#password').addClass("error");
                } else {
                    $('#password').removeClass("error");
                }
                
                if (inputError.con_password) {
                    $('#con_password').addClass("error");
                } else {
                    $('#con_password').removeClass("error");
                }

                if (inputError.first_name) {
                    $('#first_name').addClass("error");
                } else {
                    $('#first_name').removeClass("error");
                }
                
                if (inputError.last_name) {
                    $('#last_name').addClass("error");
                } else {
                    $('#last_name').removeClass("error");
                }

                if (inputError.address) {
                    $('#address').addClass("error");
                } else {
                    $('#address').removeClass("error");
                }

                if (inputError.day) {
                    $('#day').addClass("error");
                } else {
                    $('#day').removeClass("error");
                }
                if (inputError.month) {
                    $('#month').addClass("error");
                } else {
                    $('#month').removeClass("error");
                }

                if (inputError.year) {
                    $('#year').addClass("error");
                } else {
                    $('#year').removeClass("error");
                }
                
                if (inputError.sport) {
                    $('#sport').addClass("error");
                } else {
                    $('#sport').removeClass("error");
                }
                
                if (inputError.height) {
                    $('#height').addClass("error");
                } else {
                    $('#height').removeClass("error");
                }
                
                if (inputError.weight) {
                    $('#weight').addClass("error");
                } else {
                    $('#weight').removeClass("error");
                }

                if (inputError.weight_unit) {
                    $('#weight_unit').addClass("error");
                } else {
                    $('#weight_unit').removeClass("error");
                }
                                    
                if (inputError.level) {
                    $('#level').addClass("error");
                } else {
                    $('#level').removeClass("error");
                }
                
                if (inputError.gender) {
                    $('#gender').addClass("error");
                } else {
                    $('#gender').removeClass("error");
                }

                if (inputError.phone) {
                    $('#phone').addClass("error");
                } else {
                    $('#phone').removeClass("error");
                }

                if (inputError.graduation_year) {
                    $('#graduation_year').addClass("error");
                } else {
                    $('#graduation_year').removeClass("error");
                }
                
                if (inputError.profile_pic) {
                    $('#profile_pic').addClass("error");
                } else {
                    $('#profile_pic').removeClass("error");
                }

            }
        },
        beforeSend: function(){
            $(".loader").css("display",'block');
        }
    });
}

function enableDisableAdmin(admin_id,type){
    $.ajax({
        type: 'GET',
        url: 'enableDisableAdmin',
        data:"admin_id="+admin_id+"&type="+type,
        success: function(result) {
            if ($.trim(result) == 'ok') {
               window.location.href='viewAdmin';
            }else {
               location.reload();
            }
        }
    });
}

function enableDisablePlayer(player_id,type){
    $.ajax({
        type: 'GET',
        url: 'enableDisablePlayer',
        data:"player_id="+player_id+"&type="+type,
        success: function(result) {
            if ($.trim(result) == 'ok') {
               window.location.href='viewPlayer';
            }else {
                location.reload();
            }
        }
    });
}

function adminLogin(url){
	var data = $('#loginForm').serialize();
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function(result) {
            if ($.trim(result.status) == 'ok') {
               window.location.href='adminProfile';
            }else if ($.trim(result.status) == 'ng') {
               html = '<p style="color:red;font-size: 13px;">Email or Password Invalid。</p>';
			   $('#login_error_data').html(html);
            }  else {
                var inputError = result.err_field;
                console.log(inputError);

                var html = '';
				if (result.err_msg) {
					html = '<div>';
					for (var count = 0; count < result.err_msg.length; count++) {
						html += '<p style="color:red;font-size: 12px;margin:0px;">' + result.err_msg[count] + '</p>';
					}
					html += '</div>';
					$('#login_error_data').html(html);                        
					$("#login_error_data").show();
				}

                if (inputError.email) {
                    $('#login_email').addClass("error");                   
                } else {
                    $('#login_email').removeClass("error");                
                }

                if (inputError.password) {
                    $('#login_password').addClass("error");                
                } else {
                    $('#login_password').removeClass("error");                    
                }

            }
        }
    });
}

function compareRetest(retest_id,player_id){
    var level = $("#level"+retest_id).val();
    $("#retest_id").val(retest_id);
    $("#level").val(level);
    $("#playerId").val(player_id);
    if(level == ""){
        alert("Please Select Level!");
        return false;
    }
    $("#compareRetest").submit();
}

$('.profile-username').click(function() {
    $('.hidden-menu').slideToggle("slow");
    // Alternative animation for example
    // slideToggle("fast");
});

<form method="post" action="{{route('compareRetest')}}" id="compareRetest">
	@csrf
	<input name="retest_id" id="retest_id" type="hidden"/>
	<input name="level" id="level" type="hidden"/>
</form>

<div class="container profile-main" style="font-family: Oswald;">
	<div class="row">
		<div class="col-sm-12 col-md-3 col-md-offset-1">
			<div class="text-center profile-img" style="padding-top:15px;">
				<img src="@if($playerInfo[0]->filename != ''){{asset('public/uploads/player')}}/{{$playerInfo[0]->filename}}@else{{asset('public/img/default_user.jpg')}}@endif" style="border-radius: 50%; max-height: 150px;height: 170px;width: 165px;"/>
			</div>
			<div class='active-button text-center active-status'><span style="color:#000;">Available</span></div>
			<br>
			@if(count($proflBenchmarkInfo) > 0)
				<a href="{{route('test')}}" class="btn btn-primary custom-btn print-hidden" style="width:49%;color: #fff;">View Benchmark</a>
			@else
				<a href="{{route('test')}}" class="btn btn-primary custom-btn print-hidden" style="width:49%;color: #fff;"> Benchmark</a>
			@endif
			@if(count($proflBenchmarkInfo) > 0)
			<a href="{{route('retest')}}" class="btn btn-primary custom-btn print-hidden" style="width:49%;color: #fff;"> Retest</a>
			@endif
			<a href="{{route('editProfile')}}" class="btn btn-warning custom-btn print-hidden" style="width:100%;margin-top:15px;color: #fff;"><i class="fa fa-book"></i> EDIT PROFILE</a>			
		</div>

	<div class="col-sm-12 col-md-7">
			<div class="col-sm-12 col-md-12 user-mini-part" style="margin-bottom: 40px;">
				<h3 style="padding: 12px 0px 0px 0px;color: #4169e2;font-family: Oswald;margin: 0px;"><i class="fa fa-user"></i> @if(isset($playerInfo)){{$playerInfo[0]->first_name.' '.$playerInfo[0]->last_name}}@endif</h3>
				<span style="color: #000;margin:0px;">@if(isset($playerInfo)){{$playerInfo[0]->address}}@endif</span>
			</div>

			<div class="col-sm-12 col-md-6">
			<table class="table table-borderless profile-table">				 
			<tbody>
				<tr>
				<th>Date of Birth</th>
				<td>@if(isset($playerInfo)){{$playerInfo[0]->birthday}}@endif</td>
				</tr>
				<tr>
				<th>Gender</th>
				<td>@if(isset($playerInfo)){{$playerInfo[0]->gender}}@endif</td>
				</tr>
				<tr>
				<th>Height</th>
				<td>@if(isset($playerInfo)){{$playerInfo[0]->height}}@endif</td>
				</tr>
				<tr>
				<th>Weight</th>
				<td>@if(isset($playerInfo)){{$playerInfo[0]->weight}}@endif</td>
				</tr>												
							
			</tbody>
			</table>

			</div>
			<div class="col-sm-12 col-md-6 social-col">
			<table class="table table-borderless profile-table-right">				 
			<tbody>
				<tr>
				<th>Phone Number</th>
				<td>@if(isset($playerInfo)){{$playerInfo[0]->phone}}@endif</td>
				</tr>
				<tr>
				<th>Email</th>
				<td>@if(isset($playerInfo)){{$playerInfo[0]->email}}@endif</td>
				</tr>	
				<tr>
				<th>Sport</th>
				<td>@if(isset($playerInfo)){{$playerInfo[0]->sport}}@endif</td>
				</tr>
				<tr>
				<th>Graduation Year</th>
				<td>@if(isset($playerInfo)){{$playerInfo[0]->graduation_year}}@endif</td>
				</tr>								
			</tbody>
			</table>
			</div>
		</div>
	</div>

	<!-- benchmark -->
	@if(count($proflBenchmarkInfo) > 0)
	<div class="row benchmark">					
		<div class="col-sm-12 col-md-9 col-md-offset-1">
		<!--<img src="{{asset('public/img/benchmark.jpg')}}" style="width:100px;"/>-->
		<span class="benchmark-submission">Benchmark Submitted Date: {{substr($proflBenchmarkInfo[0]->inserted_date,0,10)}} Time: {{substr($proflBenchmarkInfo[0]->inserted_date,11,8)}} Level: {{$proflBenchmarkInfo[0]->level_name}} <a href="{{route('compareBenchmark')}}" style="color:blue;">Compare Data</a></span>								
		</div>		
	</div>
	@endif
	<!-- benchmark -->

</div>
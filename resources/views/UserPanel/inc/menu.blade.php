<div class="container-fluid top-header">
	<div class="container">
		<div class="row profile-header-row" style="padding-left:0px;">
			<div class="col-xs-12 col-sm-2 col-md-2 profile-header-col" style="padding-left:0px;">	
			<a href="{{route('profile')}}"><img src="{{asset('public/img/logo.gif')}}" class="responsive" /></a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-1 menu-item">
				<div class="menu-item-div">
				<ul>
					<li><a href="{{url('/profile')}}">Profile</a></li>
					<li><a href="{{url('/retest')}}">Add Re-Test</a></li>
					<li><a href="{{url('/test')}}">Benchmark</a></li>
					<li><a href="#">Support</a></li>
				</ul>
				</div>
			</div>	
			<div class="col-xs-12 col-sm-4 col-md-5 menu-right-col" style="color:#fff;padding: 0px;">		
				<div class="menu-right-section">
					<span><i class="fa fa-globe"></i> Athletic Capacity Evaluation &nbsp</span>
					BASEBALL <i class="fa fa-angle-down"></i>	
					@if(isset($playerInfo))														
						<img src="@if($playerInfo[0]->filename != ''){{asset('public/uploads/player')}}/{{$playerInfo[0]->filename}}@else{{asset('public/img/default_user.jpg')}}@endif" style="border-radius: 50%; max-height: 40px;margin-left:15px;width: 40px;height: 40px;"/>
						<span class="profile-username">{{$playerInfo[0]->user_name}} <i class="fa fa-angle-down"></i></span>
						<div class="hidden-menu" style="display: none;">
						<ul>
							<li style="padding: 10px 10px;"><a href="{{route('logout')}}" style="color:#fff;"><i class="fa fa-chevron-right"></i> Logout</a></li>						
						</ul>
						</div>
					@endif				
				</div>				
			</div>
		</div>
	</div>
</div>

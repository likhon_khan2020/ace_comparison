@if ($retestList->hasPages())
    <ul class="pagination justify-content-start float-end">
        @if ($retestList->onFirstPage())
            <li class="page-item disabled"><a class="page-link" ><i class="fa fa-angle-double-left"></i></a></li>
        @else
            <li class="page-item page-item"><a class="page-link" href="{{ $retestList->previousPageUrl() }}" rel="prev"><i class="fa fa-angle-double-left"></i></a></li>
        @endif

        @if($retestList->currentPage() > 3)
            <li class="page-item hidden-xs"><a class="page-link" href="{{ $retestList->url(1) }}">1</a></li>
        @endif
        @if($retestList->currentPage() > 4)
            <li><span style="margin-left:5px !important;padding: 0px 2px;">...</span></li>
        @endif
        @foreach(range(1, $retestList->lastPage()) as $i)
            @if($i >= $retestList->currentPage() - 2 && $i <= $retestList->currentPage() + 2)
                @if ($i == $retestList->currentPage())
                    <li class="page-item active"><a class="page-link" >{{ $i }}</a></li>
                @else
                <li class="page-item"><a class="page-link" href="{{ $retestList->url($i) }}">{{ $i }}</a></li>
                @endif
            @endif
        @endforeach
        @if($retestList->currentPage() < $retestList->lastPage() - 3)
        <li class="page-item"><span style="margin-left:5px !important;padding: 0px 2px;">...</span></li>
        @endif
        @if($retestList->currentPage() < $retestList->lastPage() - 2)
            <li class="page-item hidden-xs"><a class="page-link" href="{{ $retestList->url($retestList->lastPage()) }}">{{ $retestList->lastPage() }}</a></li>
        @endif

        @if ($retestList->hasMorePages())
        <li class="page-item"><a class="page-link" href="{{ $retestList->nextPageUrl() }}" rel="next"><i class="fa fa-angle-double-right"></i></a></li>
        @else
            <li class="disabled"><a class="page-link" ><i class="fa fa-angle-double-right"></i></a></li>
        @endif
    </ul>
@endif
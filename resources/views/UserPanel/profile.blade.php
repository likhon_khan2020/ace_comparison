@include('UserPanel.inc.header')
<body style="background-color: lightgray;">
<section class="profile">
	
	@include("UserPanel.inc.menu")
	
	@include("UserPanel.inc.profile_header")

	<div class="container retest">
		<h3 style="text-align:center;padding: 12px 0px 0px 0px;">Re-Test</h3>
		<div class="row">			
			<div class="col-sm-12 col-md-8 col-md-offset-2 retest-table-col">					
				<table class="table table-bordered table-dark">				 
				<tbody>
					<tr>
					<th class="text-center">Created Date</th>
					<th class="text-center">Created Time</th>
					<th class="text-center">Name</th>
					<th class="text-center">Level</th>
					<th class="text-center">Action</th>
					</tr>

					@if(isset($retestList))						
					@foreach($retestList as $key=>$val)
					<tr>
						<td class="text-center">{{substr($val->inserted_date,0,10)}}</td>
						<td class="text-center">{{substr($val->inserted_date,11,8)}}</td>
						<td class="text-center">Retest {{$val->test_no}}</td>
						<td class="text-center">
							<select name="level" id="level{{$val->id}}" class="form-select form-control" aria-label="Default select example" style="height:25px;min-height:25px;background:#fff;margin-bottom: 0px;">
							<option value="" selected>Select Level</option>
							<option value="1">Professional</option>
							<option value="2">Division 1</option>
							<option value="3">Division 2/ NAIA</option>
							<option value="4">Jr. College</option>
							<option value="5">HS Varsity</option>
							<option value="6">16U Athlete</option>
							<option value="7">15U Athlete</option>
							<option value="8">14U Athlete</option>
							<option value="9">13U Athlete</option>
							<option value="10">12U Athlete</option>
							<option value="11">11U Athlete</option>
							<option value="12">10U Athlete</option>
							<option value="13">9U Athlete</option>
							</select>
						</td>
						<td class="text-center"><a href="#" onclick="compareRetest({{$val->id}})" class="btn btn-primary btn-sm" style="line-height: 10px;color:#fff;">Compare</a></td>
					</tr>						
					@endforeach
					@endif

				</tbody>
				</table>	
				<div class="pagination-area mb-15 mb-sm-5 mb-lg-0">
					<nav aria-label="Page navigation example">
					@include('UserPanel/retestPagination')	
					</nav>
                </div>	
				<br>
			</div>		
		</div>
	</div>
	
	@include('UserPanel.inc.footer')


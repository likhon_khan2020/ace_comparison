@include('AdminPanel.inc.header')
<body style="background-color: lightgray;">
<section class="profile">
@include("AdminPanel.inc.menu")
	
	<div class="container add-admin-main">
		<div class="row">
			<div class="content col-sm-12 col-md-12">
			<h3 class="title text-center" id="title">@if(isset($editData)) Update Profile @else Add New Admin @endif</h3>
				<form id="registrationForm" class="contact-form" method="post" enctype='multipart/form-data' @if(isset($editData)) onsubmit="editAdmin('{{route("editAdmin")}}'); event.preventDefault();" @else onsubmit="registerAdmin('{{route("registerAdmin")}}'); event.preventDefault();" @endif style="margin-bottom: 40px;">
				@csrf
				<input name="inserted_by" value="{{$login_id}}" type="hidden"/>
				<input name="admin_id" value="@if(isset($editData)){{$editData['0']->id}}@endif" type="hidden" />
				<div id="success"></div>
				<div id="error_data" style="padding-bottom:5px;"></div>
				
				<!-- Display Success Message -->
				@if(Session::has('success_msg'))
				@php
				$success_msg = session()->get('success_msg');
				@endphp
				<div class="alert alert-success alert-dismissible " role="alert">
				    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
					<strong>{!! $success_msg !!}</strong>					
				</div>
				@endif
				<span style="display:block;">User Name</span>
				<input class="form-control" type="text" name="user_name" id="user_name" value="@if(isset($editData)){{$editData['0']->user_name}}@endif" placeholder="User Name *" />
				
				<span style="display:block;">Email</span> 
				<input class="form-control" type="email" name="email" id="email" value="@if(isset($editData)){{$editData['0']->email}}@endif" placeholder="Email *" />
				
				<div class="row" role="form">
					<div class="col-md-6">
						<span style="display:block;">Password</span>					
						<input name="password" id="password" type="password" class="form-control"  placeholder="Password @if(!isset($editData)) * @endif" />
					</div>
					<div class="col-md-6">
						<span style="display:block;">Confirm Password</span>
						<input name="con_password" id="con_password" type="password" class="form-control"  placeholder="Re-Enter Password @if(!isset($editData)) * @endif" />
					</div>
				</div>

				<span style="display:block;">First Name</span>
				<input class="form-control" type="text" name="first_name" id="first_name" value="@if(isset($editData)){{$editData['0']->first_name}}@endif" placeholder="First Name *" /> 
				
				<span style="display:block;">Last Name</span>
				<input class="form-control" type="text" name="last_name" id="last_name" value="@if(isset($editData)){{$editData['0']->last_name}}@endif" placeholder="Last Name *" /> 
				
				<div class="row" role="form">
					<div class="col-md-7 col-sm-12 col-xs-12" style="height:85px;">
						<!--<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
							<input class="form-control" id="date" name="date" placeholder="Date of Birth * (MM/DD/YYYY)" type="text"/>
						</div>-->
						<span style="display:block;">Date of Birth</span>
						<select name="day" id="day" class="form-select form-control" style="width:30%; float: left;" aria-label="Default select example">
							<option value="" selected>Select Day</option>
							@for($day = 1; $day <= 31;$day++)
							<option value="{{$day}}" @if(isset($editData) && substr($editData['0']->dob,8,2) == $day) selected @endif>{{$day}}</option>
							@endfor
						</select>
						<select name="month" id="month" class="form-select form-control" style="width:30%; float: left;margin-left: 18px;" aria-label="Default select example">
							<option value="" selected>Select Month</option>
							<option value="01" @if(isset($editData) && substr($editData['0']->dob,5,2) == '01') selected @endif>Jan</option>
							<option value="02" @if(isset($editData) && substr($editData['0']->dob,5,2) == '02') selected @endif>Feb</option>
							<option value="03" @if(isset($editData) && substr($editData['0']->dob,5,2) == '03') selected @endif>Mar</option>
							<option value="04" @if(isset($editData) && substr($editData['0']->dob,5,2) == '04') selected @endif>Apr</option>
							<option value="05" @if(isset($editData) && substr($editData['0']->dob,5,2) == '05') selected @endif>May</option>
							<option value="06" @if(isset($editData) && substr($editData['0']->dob,5,2) == '06') selected @endif>Jun</option>
							<option value="07" @if(isset($editData) && substr($editData['0']->dob,5,2) == '07') selected @endif>Jul</option>
							<option value="08" @if(isset($editData) && substr($editData['0']->dob,5,2) == '08') selected @endif>Aug</option>
							<option value="09" @if(isset($editData) && substr($editData['0']->dob,5,2) == '09') selected @endif>Sep</option>
							<option value="10" @if(isset($editData) && substr($editData['0']->dob,5,2) == '10') selected @endif>Oct</option>
							<option value="11" @if(isset($editData) && substr($editData['0']->dob,5,2) == '11') selected @endif>Nov</option>
							<option value="12" @if(isset($editData) && substr($editData['0']->dob,5,2) == '12') selected @endif>Dec</option>
						</select>			
						<select name="year" id="year" class="form-select form-control" style="width:30%;float: left;margin-left: 18px;" aria-label="Default select example">
							<option value="" selected>Select Year</option>
							@for($i = 2025; $i > 1964;$i--)
							<option value="{{$i}}" @if(isset($editData) && substr($editData['0']->dob,0,4) == $i) selected @endif>{{$i}}</option>
							@endfor
						</select>
					</div>
					<div class="col-md-5 col-sm-12 col-xs-12">
						<span style="display:block;">Phone</span>
						<input name="phone" id="phone" type="text" class="form-control" value="@if(isset($editData)){{$editData['0']->phone}}@endif"  placeholder="Phone *" />
					</div>
				</div>	
							
				<span style="display:block;">Profile Pic</span>			
				<input name="old_filename" value="@if(isset($editData)){{$editData['0']->filename}}@endif" type="hidden"/>			
				<input name="profile_pic" class="form-control form-control-lg" id="profile_pic" type="file"  accept=".jpg" />
				<div class="clearfix"></div>
				<button id="submit" class="btn btn-primary custom-btn">@if(isset($editData)) Update @else Register Now @endif</button> 
				<a class="btn btn-primary custom-btn" href="{{url('/adminProfile')}}" role="button">Go Back</a>
				<div class="loader" style="display:none"></div>
				</form>
			</div>
		</div>
	</div>
</section>

@include('AdminPanel.inc/footer')
@if ($adminList->hasPages())
    <ul class="pagination justify-content-start float-end">
        @if ($adminList->onFirstPage())
            <li class="page-item disabled"><a class="page-link" ><i class="fa fa-angle-double-left"></i></a></li>
        @else
            <li class="page-item page-item"><a class="page-link" href="{{ $adminList->previousPageUrl() }}" rel="prev"><i class="fa fa-angle-double-left"></i></a></li>
        @endif

        @if($adminList->currentPage() > 3)
            <li class="page-item hidden-xs"><a class="page-link" href="{{ $adminList->url(1) }}">1</a></li>
        @endif
        @if($adminList->currentPage() > 4)
            <li><span style="margin-left:5px !important;padding: 0px 2px;">...</span></li>
        @endif
        @foreach(range(1, $adminList->lastPage()) as $i)
            @if($i >= $adminList->currentPage() - 2 && $i <= $adminList->currentPage() + 2)
                @if ($i == $adminList->currentPage())
                    <li class="page-item active"><a class="page-link" >{{ $i }}</a></li>
                @else
                <li class="page-item"><a class="page-link" href="{{ $adminList->url($i) }}">{{ $i }}</a></li>
                @endif
            @endif
        @endforeach
        @if($adminList->currentPage() < $adminList->lastPage() - 3)
        <li class="page-item"><span style="margin-left:5px !important;padding: 0px 2px;">...</span></li>
        @endif
        @if($adminList->currentPage() < $adminList->lastPage() - 2)
            <li class="page-item hidden-xs"><a class="page-link" href="{{ $adminList->url($adminList->lastPage()) }}">{{ $adminList->lastPage() }}</a></li>
        @endif

        @if ($adminList->hasMorePages())
        <li class="page-item"><a class="page-link" href="{{ $adminList->nextPageUrl() }}" rel="next"><i class="fa fa-angle-double-right"></i></a></li>
        @else
            <li class="disabled"><a class="page-link" ><i class="fa fa-angle-double-right"></i></a></li>
        @endif
    </ul>
@endif
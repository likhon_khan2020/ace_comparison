@include('UserPanel.inc.header')
<body style="background-color: lightgray;">
<section>

@include("UserPanel.inc.menu")

@include("UserPanel.inc.profile_header")

	<div class="container dashboard-bemchmark-main">
		<div class="row">
			<div class="col-sm-12 col-md-12" style="color:#fff;">	
			<div style="color:#000;text-align: center;margin-top:15px;margin-bottom:15px;"><h3>Compare Benchmark</h3></div>			
				<div class="table-responsive">
					<table class="table table-bordered dashboard-header">
					<tbody>
					<tr>
						<td style="font-weight:bold;">Benchmark Testing Date</td>
						<td>{{substr($benchmarkInfo['inserted_date'],0,10)}}</td>
						<td style="font-weight:bold;">Improved</td>
						<td style="width:12%;background:#95D195"></td>
					</tr>
					<tr>
						<td style="font-weight:bold;">Re-Testing Date</td>
						<td>{{substr($retestInfo['inserted_date'],0,10)}}</td>
						<td style="font-weight:bold;">Stayed The Same</td>
						<td style="width:12%;background:#FAFB57"></td>
					</tr>
					<tr>
						@php
						$earlier = new DateTime(substr($benchmarkInfo['inserted_date'],0,10));
						$later = new DateTime(substr($retestInfo['inserted_date'],0,10));
						$pos_diff = $earlier->diff($later)->format("%r%a"); 
						@endphp
						<td style="font-weight:bold;"># Of Days Between Testing</td>
						<td>{{$pos_diff}}</td>
						<td style="font-weight:bold;">Declined</td>
						<td style="width:12%;background:#F65E5D"></td>
					</tr>
					
					</tbody>
				</table>
				</div>		
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12 col-md-12" style="color:#fff;">	
				<div class="table-responsive">
					<table class="table table-striped dashboard-table">
						<thead>
							<tr>
							<th style="background: lightgray;">Station</th>
							<th style="background: lightgray;">Benchmark Testing</th>
							<th style="background: lightgray;">Re-Testing</th>
							<th style="background: cornflowerblue;">Improvement</th>
							<th style="background: lightgray;">Goal</th>
							</tr>
						</thead>
						<tbody>
						<tr>
								<td>10 yd Sprint (sec)</td>
								<td>{{$benchmarkInfo['10_yd_sprint']}}</td>
								<td>{{$retestInfo['10_yd_sprint']}}</td>
								<td @if($retestInfo['10_yd_sprint'] == 0) style={{'background:#FAFB57'}} @elseif($retestInfo['10_yd_sprint'] < $benchmarkInfo['10_yd_sprint']) style={{'background:#95D195'}} @elseif($retestInfo['10_yd_sprint'] > $benchmarkInfo['10_yd_sprint'] ) style={{'background:#F65E5D'}} @elseif(($retestInfo['10_yd_sprint'] == $benchmarkInfo['10_yd_sprint'])) style={{'background:#FAFB57'}}  @endif>
									@if($retestInfo['10_yd_sprint'] == 0) 
									{{$benchmarkInfo['10_yd_sprint']}}
									@else
									{{$retestInfo['10_yd_sprint'] - $benchmarkInfo['10_yd_sprint']}}
									@endif
								</td>
								<td class="goal-td-bg">@if(isset($goalInfo['10_yd_sprint'])){{$goalInfo['10_yd_sprint']}}@endif</td>
							</tr>
							
							<tr>
								<td>40 yd Sprint (sec)</td>
								<td>{{$benchmarkInfo['40_yd_sprint']}}</td>
								<td>{{$retestInfo['40_yd_sprint']}}</td>
								<td @if($retestInfo['40_yd_sprint'] == 0) style={{'background:#FAFB57'}} @elseif($retestInfo['40_yd_sprint'] < $benchmarkInfo['40_yd_sprint']) style={{'background:#95D195'}} @elseif($retestInfo['40_yd_sprint'] > $benchmarkInfo['40_yd_sprint'] ) style={{'background:#F65E5D'}} @elseif(($retestInfo['40_yd_sprint'] == $benchmarkInfo['40_yd_sprint'])) style={{'background:#FAFB57'}}  @endif>
									@if($retestInfo['40_yd_sprint'] == 0) 
									{{$benchmarkInfo['40_yd_sprint']}}
									@else
									{{$retestInfo['40_yd_sprint'] - $benchmarkInfo['40_yd_sprint']}}
									@endif
								</td>
								<td class="goal-td-bg">@if(isset($goalInfo['40_yd_sprint'])){{$goalInfo['40_yd_sprint']}}@endif</td>
							</tr>
							<tr>
								<td>60 yd Sprint (sec)</td>
								<td>{{$benchmarkInfo['60_yd_sprint']}}</td>
								<td>{{$retestInfo['60_yd_sprint']}}</td>
								<td @if($retestInfo['60_yd_sprint'] == 0) style={{'background:#FAFB57'}} @elseif($retestInfo['60_yd_sprint'] < $benchmarkInfo['60_yd_sprint']) style={{'background:#95D195'}} @elseif($retestInfo['60_yd_sprint'] > $benchmarkInfo['60_yd_sprint'] ) style={{'background:#F65E5D'}} @elseif(($retestInfo['60_yd_sprint'] == $benchmarkInfo['60_yd_sprint'])) style={{'background:#FAFB57'}}  @endif>
									@if($retestInfo['60_yd_sprint'] == 0) 
									{{$benchmarkInfo['60_yd_sprint']}}
									@else
									{{$retestInfo['60_yd_sprint'] - $benchmarkInfo['60_yd_sprint']}}
									@endif
								</td>
								<td class="goal-td-bg">@if(isset($goalInfo['60_yd_sprint'])){{$goalInfo['60_yd_sprint']}}@endif</td>
							</tr>

							<tr>
								<td>Grip Strength (lbs)</td>
								<td>{{$benchmarkInfo['grip_strength']}}</td>
								<td>{{$retestInfo['grip_strength']}}</td>
								<td @if($retestInfo['grip_strength'] == 0) style={{'background:#FAFB57'}} @elseif($retestInfo['grip_strength'] - $benchmarkInfo['grip_strength'] < 0) style={{'background:#F65E5D'}} @elseif(($retestInfo['grip_strength'] - $benchmarkInfo['grip_strength']) == 0) style={{'background:#FAFB57'}} @else style={{'background:#95D195'}} @endif>
									@if($retestInfo['grip_strength'] == 0) 
									{{$benchmarkInfo['grip_strength']}}
									@else
									{{$retestInfo['grip_strength'] - $benchmarkInfo['grip_strength']}}
									@endif
								</td>
								<td class="goal-td-bg">@if(isset($goalInfo['grip_strength'])){{$goalInfo['grip_strength']}}@endif</td>
							</tr>
							
							<tr>
								<td>Broad Jump (total in)</td>
								<td>{{$benchmarkInfo['broad_jump']}}</td>
								<td>{{$retestInfo['broad_jump']}}</td>
								<td @if($retestInfo['broad_jump'] == 0) style={{'background:#FAFB57'}} @elseif($retestInfo['broad_jump'] - $benchmarkInfo['broad_jump'] < 0) style={{'background:#F65E5D'}} @elseif(($retestInfo['broad_jump'] - $benchmarkInfo['broad_jump']) == 0) style={{'background:#FAFB57'}} @else style={{'background:#95D195'}} @endif>
									@if($retestInfo['broad_jump'] == 0) 
									{{$benchmarkInfo['broad_jump']}}
									@else
									{{$retestInfo['broad_jump'] - $benchmarkInfo['broad_jump']}}
									@endif
								</td>
								<td class="goal-td-bg">@if(isset($goalInfo['broad_jump'])){{$goalInfo['broad_jump']}}@endif</td>
							</tr>

							<tr>
								<td>Vertical Jump (in)</td>
								<td>{{$benchmarkInfo['vertical_jump']}}</td>
								<td>{{$retestInfo['vertical_jump']}}</td>
								<td @if($retestInfo['vertical_jump'] == 0) style={{'background:#FAFB57'}} @elseif($retestInfo['vertical_jump'] - $benchmarkInfo['vertical_jump'] < 0) style={{'background:#F65E5D'}} @elseif(($retestInfo['vertical_jump'] - $benchmarkInfo['vertical_jump']) == 0) style={{'background:#FAFB57'}} @else style={{'background:#95D195'}} @endif>
									@if($retestInfo['vertical_jump'] == 0) 
									{{$benchmarkInfo['vertical_jump']}}
									@else
									{{$retestInfo['vertical_jump'] - $benchmarkInfo['vertical_jump']}}
									@endif
								</td>
								<td class="goal-td-bg">@if(isset($goalInfo['vertical_jump'])){{$goalInfo['vertical_jump']}}@endif</td>
							</tr>

							<tr>
								<td>Shoulder Flex (in)</td>
								<td>{{$benchmarkInfo['ss_shoulder_flex']}}</td>
								<td>{{$retestInfo['ss_shoulder_flex']}}</td>
								<td @if($retestInfo['ss_shoulder_flex'] == 0) style={{'background:#FAFB57'}} @elseif($retestInfo['ss_shoulder_flex'] < $benchmarkInfo['ss_shoulder_flex']) style={{'background:#95D195'}} @elseif($retestInfo['ss_shoulder_flex'] > $benchmarkInfo['ss_shoulder_flex'] ) style={{'background:#F65E5D'}} @elseif(($retestInfo['ss_shoulder_flex'] == $benchmarkInfo['ss_shoulder_flex'])) style={{'background:#FAFB57'}} @endif>
									@if($retestInfo['ss_shoulder_flex'] == 0) 
									{{$benchmarkInfo['ss_shoulder_flex']}}
									@else
									{{$retestInfo['ss_shoulder_flex'] - $benchmarkInfo['ss_shoulder_flex']}}
									@endif
								</td>
								<td class="goal-td-bg">@if(isset($goalInfo['ss_shoulder_flex'])){{$goalInfo['ss_shoulder_flex']}}@endif</td>
							</tr>
							
							<tr>
								<td>Pro Agility Right (sec)</td>
								<td>{{$benchmarkInfo['pro_agility']}}</td>
								<td>{{$retestInfo['pro_agility']}}</td>
								<td @if($retestInfo['pro_agility'] == 0) style={{'background:#FAFB57'}} @elseif($retestInfo['pro_agility'] < $benchmarkInfo['pro_agility']) style={{'background:#95D195'}} @elseif($retestInfo['pro_agility'] > $benchmarkInfo['pro_agility'] ) style={{'background:#F65E5D'}} @elseif(($retestInfo['pro_agility'] == $benchmarkInfo['pro_agility'])) style={{'background:#FAFB57'}}  @endif>
									@if($retestInfo['pro_agility'] == 0) 
									{{$benchmarkInfo['pro_agility']}}
									@else
									{{$retestInfo['pro_agility'] - $benchmarkInfo['pro_agility']}}
									@endif
								</td>
								<td class="goal-td-bg">@if(isset($goalInfo['pro_agility'])){{$goalInfo['pro_agility']}}@endif</td>
							</tr>

							<tr>
								<td>Throwing Velocity (mph)</td>
								<td>{{$benchmarkInfo['throwing_velocity']}}</td>
								<td>{{$retestInfo['throwing_velocity']}}</td>
								<td @if($retestInfo['throwing_velocity'] == 0) style={{'background:#FAFB57'}} @elseif($retestInfo['throwing_velocity'] - $benchmarkInfo['throwing_velocity'] < 0) style={{'background:#F65E5D'}} @elseif(($retestInfo['throwing_velocity'] - $benchmarkInfo['throwing_velocity']) == 0) style={{'background:#FAFB57'}} @else style={{'background:#95D195'}} @endif>
									@if($retestInfo['throwing_velocity'] == 0) 
									{{$benchmarkInfo['throwing_velocity']}}
									@else
									{{$retestInfo['throwing_velocity'] - $benchmarkInfo['throwing_velocity']}}
									@endif
								</td>
								<td class="goal-td-bg">@if(isset($goalInfo['throwing_velocity'])){{$goalInfo['throwing_velocity']}}@endif</td>
							</tr>

							<tr>
								<td>Exit Velocity (mph)</td>
								<td>{{$benchmarkInfo['exit_velocity']}}</td>
								<td>{{$retestInfo['exit_velocity']}}</td>
								<td @if($retestInfo['exit_velocity'] == 0) style={{'background:#FAFB57'}} @elseif($retestInfo['exit_velocity'] - $benchmarkInfo['exit_velocity'] < 0) style={{'background:#F65E5D'}} @elseif(($retestInfo['exit_velocity'] - $benchmarkInfo['exit_velocity']) == 0) style={{'background:#FAFB57'}} @else style={{'background:#95D195'}} @endif>
									@if($retestInfo['exit_velocity'] == 0) 
									{{$benchmarkInfo['exit_velocity']}}
									@else
									{{$retestInfo['exit_velocity'] - $benchmarkInfo['exit_velocity']}}
									@endif
								</td>
								<td class="goal-td-bg">@if(isset($goalInfo['exit_velocity'])){{$goalInfo['exit_velocity']}}@endif</td>
							</tr>						
						
							<tr>
								<td>Stand And Reach (in)</td>
								<td>{{$benchmarkInfo['stand_and_reach']}}</td>
								<td>{{$retestInfo['stand_and_reach']}}</td>
								<td @if($retestInfo['stand_and_reach'] == 0) style={{'background:#FAFB57'}} @elseif($retestInfo['stand_and_reach'] < $benchmarkInfo['stand_and_reach']) style={{'background:#95D195'}} @elseif($retestInfo['stand_and_reach'] > $benchmarkInfo['stand_and_reach'] ) style={{'background:#F65E5D'}} @elseif(($retestInfo['stand_and_reach'] == $benchmarkInfo['stand_and_reach'])) style={{'background:#FAFB57'}}  @endif>
									@if($retestInfo['stand_and_reach'] == 0) 
									{{$benchmarkInfo['stand_and_reach']}}
									@else
									{{$retestInfo['stand_and_reach'] - $benchmarkInfo['stand_and_reach']}}
									@endif
								</td>
								<td class="goal-td-bg">@if(isset($goalInfo['stand_and_reach'])){{$goalInfo['stand_and_reach']}}@endif</td>
							</tr>

							<tr>
								<td>HTPS- R (sec)</td>
								<td>{{$benchmarkInfo['htps']}}</td>
								<td>{{$retestInfo['htps']}}</td>
								<td @if($retestInfo['htps'] == 0) style={{'background:#FAFB57'}} @elseif($retestInfo['htps'] < $benchmarkInfo['htps']) style={{'background:#95D195'}} @elseif($retestInfo['htps'] > $benchmarkInfo['htps'] ) style={{'background:#F65E5D'}} @elseif(($retestInfo['htps'] == $benchmarkInfo['htps'])) style={{'background:#FAFB57'}}  @endif>
									@if($retestInfo['htps'] == 0) 
									{{$benchmarkInfo['htps']}}
									@else
									{{$retestInfo['htps'] - $benchmarkInfo['htps']}}
									@endif
								</td>
								<td class="goal-td-bg">@if(isset($goalInfo['htps'])){{$goalInfo['htps']}}@endif</td>
							</tr>
							
						</tbody>
					</table>	
				</div>		
			</div>
		</div>
	</div>		
</section>

@include("UserPanel.inc.footer");

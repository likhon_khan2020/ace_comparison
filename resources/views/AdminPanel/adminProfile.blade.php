@include('AdminPanel.inc.header')
<body style="background-color: lightgray;">
<section class="profile">
	
	@include("AdminPanel.inc.menu")
	
	@include("AdminPanel.inc.profile_header")

	<div class="container retest">
		<h3 style="text-align:center;padding: 12px 0px 0px 0px;">Re-Test</h3>		
		<div class="row">			
			<div class="col-sm-12 col-md-8 col-md-offset-2 retest-table-col">

			<div class="player-search">
				<form method="get" action='{{route("adminProfile")}}'> 
					<select name="player_id" class="form-select form-control" id="playerList" aria-label="Default select example">
						<option value="">Select Player</option>
						@foreach($playerList as $key=>$val)
						<option value="{{$val->id}}" @if(request('player_id')  == $val->id) selected @endif>{{$val->first_name .' '.$val->last_name}}</option>
						@endforeach
					</select>
					<br><br>
					<div class="input-group player-search-start-date">
						<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
						<input class="form-control" id="start_date" name="start_date" value="@if(request('start_date')) {{request('start_date')}} @endif" placeholder="Date of Birth * (YYYY-MM-DD)" type="text"/>
					</div>
					<span style="float: left;padding-top:5px;"> &nbsp - &nbsp </span>
					<div class="input-group player-search-end-date">
						<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
						<input class="form-control" id="end_date" name="end_date" value="@if(request('end_date')) {{request('end_date')}} @endif" placeholder="Date of Birth * (YYYY-MM-DD)" type="text"/>
					</div>

					<button class="btn btn-success player-search-button">Search</button>
				</form>
			</div>

				<div class="table-responsive">
					<table class="table table-bordered table-dark">				 
					<tbody>
						<tr>
						<th class="text-center">Submitted Date</th>
						<th class="text-center">Submitted Time</th>
						<th class="text-center">Name</th>
						<th class="text-center">Submitted by</th>
						<th class="text-center">Player Level</th>
						<th class="text-center">Action</th>
						</tr>

						@if(isset($retestList))							
						@foreach($retestList as $key=>$val)
						<tr>
							<td class="text-center">{{substr($val->inserted_date,0,10)}}</td>
							<td class="text-center">{{substr($val->inserted_date,11,8)}}</td>
							<td class="text-center">@if($val->test_status == 'benchmark'){{'Benchmark'}}@else Retest {{$val->test_no}}@endif</td>
							<td class="text-center">{{$val->player_name}}</td>
							<td class="text-center">
								<select name="level" id="level{{$val->id}}" class="form-select form-control" aria-label="Default select example" style="height:25px;min-height:25px;background:#fff;margin-bottom: 0px;">
								<option value="" selected>Select Level</option>
								<option value="1">Professional</option>
								<option value="2">Division 1</option>
								<option value="3">Division 2/ NAIA</option>
								<option value="4">Jr. College</option>
								<option value="5">HS Varsity</option>
								<option value="6">16U Athlete</option>
								<option value="7">15U Athlete</option>
								<option value="8">14U Athlete</option>
								<option value="9">13U Athlete</option>
								<option value="10">12U Athlete</option>
								<option value="11">11U Athlete</option>
								<option value="12">10U Athlete</option>
								<option value="13">9U Athlete</option>
								</select>
							</td>
							<td class="text-center"><a href="#" onclick="compareRetest({{$val->id}},{{$val->player_id}})" class="btn btn-primary btn-sm" style="line-height: 10px;color:#fff;">Compare</a></td>
						</tr>							
						@endforeach
						@endif

					</tbody>
					</table>
				</div>
				<div class="pagination-area mb-15 mb-sm-5 mb-lg-0">
					<nav aria-label="Page navigation example">
					@include('UserPanel/retestPagination')	
					</nav>
                </div>	
				<br>
			</div>		
		</div>
	</div>
	
	@include('AdminPanel.inc/footer')

	<script>
		$(document).ready(function() {
		$('#playerList').select2();
	});
	</script>


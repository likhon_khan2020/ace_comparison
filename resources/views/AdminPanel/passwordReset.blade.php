	@include('AdminPanel.inc.header')
    <body>
   
    @include("AdminPanel.inc.menu")

        <div class="retest-page-header">
            <div class="container-fluid create-retest-container" style="background-image: url({{url('/')}}/public/img/bg-3.jpg);">
                <h1 class="title text-center" style="color:#fff;">Password Reset</h1>
            </div>
        </div>

        <!-- page-header -->
        <section class="page-section">
            <div class="container">
                <div class="row">
                    <div class="content col-sm-12 col-md-8 col-md-offset-2">
                        
                    <script>
                        var base_url = "{{url('/')}}";
                    </script>
                        <form id="passwordResetForm" class="contact-form" method="post" enctype='multipart/form-data' onsubmit="passwordReset('{{route("admin.passwordResetPost")}}'); event.preventDefault();">
						@csrf
                        <input type="hidden" name="admin_id" id="admin_id" value="@if(isset($adminInfo)){{$adminInfo['0']->id}}@endif" readonly />

                        <div id="success"></div>
                        <div id="error_data" style="padding-bottom:5px;"></div>
						
						<!-- Display Success Message -->
						@if(Session::has('success_msg'))
						@php
						$success_msg = session()->get('success_msg');
						@endphp
						<div class="alert alert-success alert-dismissible " role="alert">
						  <strong>{{$success_msg}}</strong>
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						  </button>
						</div>
						@endif
						<span style="display:block;">Email</span>
                        <input class="form-control" type="text" name="email" id="email" value="@if(isset($adminInfo)){{$adminInfo['0']->email}}@endif" readonly />
                       
                        <span style="display:block;">New Password</span> 
                        <input class="form-control" type="password" name="password" id="password"  placeholder="Password *" />

                        <span style="display:block;">New Password</span> 
                        <input class="form-control" type="password" name="con_password" id="con_password"  placeholder="Re-Enter Password *" />
						                
                        <div class="clearfix"></div>
                        <button id="submit" class="btn btn-default" style="background: green;color: #fff;">Update </button> 
                        
                        <!-- .buttons-box --></form>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- page-section -->
        
    @include('AdminPanel.inc.footer')

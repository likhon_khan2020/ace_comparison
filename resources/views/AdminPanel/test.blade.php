	@include('UserPanel/inc/header')
    <body style="background-color: lightgray;">
    @include("UserPanel.inc.menu")
        <div class="benchmark-page-header">
            <div class="container-fluid create-benchmark-container" style="background-image: url({{url('/')}}/public/img/bg-3.jpg);">
                <h1 class="title text-center" style="color:#fff;">Benchmark Testing</h1>
            </div>
        </div>
        <!-- page-header -->
        <section>
            <div class="container create-benchmark">
                <div class="row">
                    <div class="content col-sm-12 col-md-8 col-md-offset-2">
                        <form id="testingForm" class="contact-form" method="post" enctype='multipart/form-data' onsubmit="registerTesting('{{route("registerTesting")}}'); event.preventDefault();">
						@csrf    
                        <input name="player_id" value="{{$player_id}}" type="hidden"/>                  
                        <div id="success"></div>
                        <div id="error_data" style="padding-bottom:5px;"></div>
						
						<!-- Display Success Message -->
						@if(Session::has('success_msg'))
						@php
						$success_msg = session()->get('success_msg');
						@endphp
						<div class="alert alert-success alert-dismissible " role="alert">
						  <strong>{{$success_msg}}</strong>
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						  </button>
						</div>
						@endif
					
						<div class="row" role="form" style="margin-bottom:15px;">
                            <div class="col-md-4">
                                <span style="font-weight:bold;">Station</span>
                            </div>
                            <div class="col-md-8">
                                <span style="font-weight:bold;">Benchmark</span>
                            </div>
                        </div>
						
                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                10 yd Sprint (sec)							   
                            </div>
                            <div class="col-md-8">
                                <input name="10_yd_sprint" id="10_yd_sprint" type="text" class="form-control"  placeholder="sec" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['10_yd_sprint']}}" readonly @endif />
                            </div>
                        </div>						
						
                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                40 yd Sprint (sec)								   
                            </div>
                            <div class="col-md-8">
                                <input name="40_yd_sprint" id="40_yd_sprint" type="text" class="form-control"  placeholder="sec" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['40_yd_sprint']}}" readonly @endif/>
                            </div>
                        </div>
                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                60 yd Sprint (sec)								   
                            </div>
                            <div class="col-md-8">
                                <input name="60_yd_sprint" id="60_yd_sprint" type="text" class="form-control"  placeholder="sec" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['60_yd_sprint']}}" readonly @endif />
                            </div>
                        </div>

                        <div class="row" role="form">
                            <div class="col-md-4">                               
                            Grip Strength (lbs)							   
                            </div>
                            <div class="col-md-8">
                                <input name="grip_strength" id="grip_strength" type="text" class="form-control"  placeholder="lbs" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['grip_strength']}}" readonly @endif />
                            </div>
                        </div>
                       
                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                Broad Jump (total in)							   
                            </div>
                            <div class="col-md-8">
                                <input name="broad_jump" id="broad_jump" type="text" class="form-control"  placeholder="total in" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['broad_jump']}}" readonly @endif />
                            </div>
                        </div>

                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                Vertical Jump (in)						   
                            </div>
                            <div class="col-md-8">
                                <input name="vertical_jump" id="vertical_jump" type="text" class="form-control"  placeholder="in" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['vertical_jump']}}" readonly @endif  />
                            </div>
                        </div>

                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                Shoulder Flex (in)							   
                            </div>
                            <div class="col-md-8">
                                <input name="ss_shoulder_flex" id="ss_shoulder_flex" type="text" class="form-control"  placeholder="in" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['ss_shoulder_flex']}}" readonly @endif />
                            </div>
                        </div>
                   
                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                Pro Agility (sec)							   
                            </div>
                            <div class="col-md-8">
                                <input name="pro_agility" id="pro_agility" type="text" class="form-control"  placeholder="sec" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['pro_agility']}}" readonly @endif />
                            </div>
                        </div>

                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                Throwing Velocity (mph)							   
                            </div>
                            <div class="col-md-8">
                                <input name="throwing_velocity" id="throwing_velocity" type="text" class="form-control"  placeholder="mph" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['throwing_velocity']}}" readonly @endif />
                            </div>
                        </div>

                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                Exit Velocity (mph)						   
                            </div>
                            <div class="col-md-8">
                                <input name="exit_velocity" id="exit_velocity" type="text" class="form-control"  placeholder="mph" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['exit_velocity']}}" readonly @endif />
                            </div>
                        </div>
                                                
                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                Stand And Reach	(in)						   
                            </div>
                            <div class="col-md-8">
                                <input name="stand_and_reach" id="stand_and_reach" type="text" class="form-control"  placeholder="in" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['stand_and_reach']}}" readonly @endif />
                            </div>
                        </div>

                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                HTPS (sec)						   
                            </div>
                            <div class="col-md-8">
                                <input name="htps" id="htps" type="text" class="form-control"  placeholder="sec" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['htps']}}" readonly @endif />
                            </div>
                        </div>
                       
                        <div class="clearfix"></div>
                        @if(!isset($benchmarkInfo))
                        <button id="submit" class="btn btn-default custom-btn">Submit</button> 
                        @endif
                        <a class="btn btn-primary custom-btn" href="{{url('/profile')}}" role="button">Goto Profile</a>
                        </form>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- page-section -->
        
    @include('UserPanel/inc/footer')

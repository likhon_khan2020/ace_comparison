<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Athletic Capacity Evaluation</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Player Record HTML Template" />
	<meta name="author" content="playerrecord.com" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<!-- Favicon -->
	<link rel="shortcut icon" href="{{asset('public/img/favicon.ico')}}" />
	<!-- Font -->
	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Arimo:300,400,700,400italic,700italic' />
	<link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css' />
	<!-- Font Awesome Icons -->
	<link href="{{asset('public/css/font-awesome.min.css')}}" rel='stylesheet' type='text/css' />
	<!-- Bootstrap core CSS -->
	<link href="{{asset('public/css/bootstrap.min.css')}}" rel="stylesheet" />
	<link href="{{asset('public/css/hover-dropdown-menu.css')}}" rel="stylesheet" />
	<!-- Icomoon Icons -->
	<link href="{{asset('public/css/icons.css')}}" rel="stylesheet" />
  
	<!-- Animations -->
	<link href="{{asset('public/css/animate.min.css')}}" rel="stylesheet" />
	
	<!-- Custom Style -->
	<link href="{{asset('public/css/select2.css')}}" rel="stylesheet" />
	<link href="{{asset('public/css/style.css')}}" rel="stylesheet" />
   <link href="{{asset('public/css/responsive.css')}}" rel="stylesheet" />
   <link href="{{asset('public/AdminPanel/custom.css')}}" rel="stylesheet" />
</head>
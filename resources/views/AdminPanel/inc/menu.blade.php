<div class="container-fluid top-header">
	<div class="container">
		<div class="row profile-header-row" style="padding-left:0px;">
			<div class="col-xs-12 col-sm-3 col-md-1 profile-header-col" style="padding-left:0px;">	
			<a href="{{route('adminProfile')}}"><img src="{{asset('public/img/logo.gif')}}" class="responsive" /></a>
			</div>
			<div class="col-xs-12 col-sm-9 col-md-5 col-md-offset-6 menu-right-col" style="color:#fff;padding: 0px;">		
				<div class="menu-right-section">
					<span><i class="fa fa-dribbble"></i>Sport &nbsp</span>
					BASEBALL <i class="fa fa-angle-down"></i>															
					<img src="@if($adminInfo[0]->filename != ''){{asset('public/uploads/admin')}}/{{$adminInfo[0]->filename}}@else{{asset('public/img/default_user.jpg')}}@endif" style="border-radius: 50%; max-height: 40px;margin-left:15px;width: 40px;height: 40px;"/>
					<span class="profile-username">@if(isset($playerInfo)){{$playerInfo[0]->user_name}}@endif <i class="fa fa-angle-down"></i></span>
					<div class="hidden-menu" style="display: none;">
					<ul>
						<li style="padding: 10px 10px;"><a href="{{route('adminLogout')}}" style="color:#fff;"><i class="fa fa-chevron-right"></i> Logout</a></li>						
					</ul>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
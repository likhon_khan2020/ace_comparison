<form method="post" action="{{route('compareBenchmark')}}" id="compareBenchmark">
	@csrf
	<input name="level_id" id="benchmarkLevel" type="hidden"/>
</form>

<form method="post" action="{{route('admin.compareRetest')}}" id="compareRetest">
	@csrf
	<input name="retest_id" id="retest_id" type="hidden"/>
	<input name="level" id="level" type="hidden"/>
	<input name="player_id" id="playerId" type="hidden"/>
</form>

<div class="container profile-main" style="font-family: Oswald;">
	<div class="row">
		<div class="col-sm-12 col-md-3 col-md-offset-1">
			<div class="text-center profile-img" style="padding-top:15px;">
				<img src="@if($adminInfo[0]->filename != ''){{asset('public/uploads/admin')}}/{{$adminInfo[0]->filename}}@else{{asset('public/img/default_user.jpg')}}@endif" style="border-radius: 50%; max-height: 150px;height: 170px;width: 165px;"/>
			</div>
			<div class='active-button text-center active-status'><span style="color:#000;">Available</span></div>			
			<a href="{{route('editAdminProfile')}}" class="btn btn-warning custom-btn profile-edit-button" style="width:100%;margin-top:24px;color: #fff;"><i class="fa fa-book"></i> EDIT PROFILE</a>			
		</div>
		<div class="col-sm-12 col-md-7">
			<div class="col-sm-12 col-md-12 user-mini-part">
				<h3 style="padding: 12px 0px 0px 0px;color: #4169e2;font-family: Oswald;margin: 0px;"><i class="fa fa-user"></i> @if(isset($adminInfo)){{$adminInfo[0]->first_name.' '.$adminInfo[0]->last_name}}@endif</h3>
				<span style="color: #000;margin:0px;">Dhaka,Bangladesh.</span>
			</div>

			<div class="col-sm-12 col-md-8" style="margin-top: 15px;">		
				<table class="table table-borderless profile-table">	
					<tbody>
						<tr>
						<th>Date of Birth</th>
						<td>@if(isset($adminInfo)){{$adminInfo[0]->dob}}@endif</td>
						</tr>			
						<tr>
						<th>Phone Number</th>
						<td>@if(isset($adminInfo)){{$adminInfo[0]->phone}}@endif</td>
						</tr>
						<tr>
						<th>Email</th>
						<td>@if(isset($adminInfo)){{$adminInfo[0]->email}}@endif</td>
						</tr>												
					</tbody>
				</table>
				
			</div>

			<div class="col-sm-12 col-md-4 social-col" style="padding:0px;">
				<a href="{{route('viewAdmin')}}" class="btn btn-primary custom-btn view-admin-btn" style="width:100%;color: #fff;">View Admin</a>			
				<a href="{{route('addAdmin')}}" class="btn btn-primary custom-btn add-admin-btn" style="width:100%;margin-top:5px;color: #fff;"> Add New Admin</a>
				<a href="{{route('viewPlayer')}}" class="btn btn-primary custom-btn view-player-btn" style="width:100%;margin-top:5px;color: #fff;">View Player</a>			
				<a href="{{route('admin.playerRegister')}}" class="btn btn-primary custom-btn add-player-btn" style="width:100%;margin-top:5px;color: #fff;"> Add New Player</a>						
			</div>
		</div>
	</div>
</div>
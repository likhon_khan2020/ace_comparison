<!-- Top Bar -->
<div id="top-bar" class="top-bar-section top-bar-bg-color">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<!-- Top Contact -->
				<div class="top-contact link-hover-black">
				<a href="#">
				<i class="fa fa-phone"></i>+ 123 132 1234</a> 
				<a href="#">
				<i class="fa fa-envelope"></i>info@playerrecord.com</a></div>
				<!-- Top Social Icon -->
				<div class="top-social-icon icons-hover-black">						
				<a href="#">
					<i class="fa fa-facebook"></i>
				</a> 
				<a href="#">
					<i class="fa fa-twitter"></i>
				</a> 
				<a href="#">
					<i class="fa fa-youtube"></i>
				</a>                        
				<a href="#">
					<i class="fa fa-linkedin"></i>
				</a>                                               
				<a href="#">
					<i class="fa fa-google-plus"></i>
				</a>						
				
				</div>
				
			</div>
		</div>
	</div>
</div>
<!-- Top Bar -->
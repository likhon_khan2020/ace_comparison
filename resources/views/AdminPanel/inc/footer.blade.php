        <!-- footer -->
        <footer class="common_footer">            	      
            <div class="container">
                <div class="row" style="padding-top: 5px;">                        
                   <!-- <div class="col-xs-10 col-sm-4 col-md-3 col-md-offset-4 col-sm-offset-2 footer-left">
                        <span>ACE COMPARISON PROJECT</span>	            
                        <span style="display:block;position:relative;bottom: 8px;font-size: 13px;">Email: ace_comparison@gmail.com</span>					
                    </div>-->
                <div class="col-xs-12 col-sm-12 col-md-12"> 
                    <p style="text-align: center;">&copy; 2021 <a href="https://globalsportsmetrics.com/" style="color:#fff;">globalsportsmetrics.com</a>.</p>
                </div>
            </div>       
        </footer>
        <!-- footer -->
    </div>
    <!-- page -->
    <!-- Scripts -->
    <script type="text/javascript" src="{{asset('public/js/jquery.min.js')}}"></script> 
    <script type="text/javascript" src="{{asset('public/js/bootstrap.min.js')}}"></script> 
    <!-- Menu jQuery plugin -->
     
    <script type="text/javascript" src="{{asset('public/js/hover-dropdown-menu.js')}}"></script> 
    <!-- Menu jQuery Bootstrap Addon --> 
    <script type="text/javascript" src="{{asset('public/js/jquery.hover-dropdown-menu-addon.js')}}"></script> 
    <!-- Scroll Top Menu -->
     
    <script type="text/javascript" src="{{asset('public/js/jquery.easing.1.3.js')}}"></script> 
    <!-- Sticky Menu --> 
    <script type="text/javascript" src="{{asset('public/js/jquery.sticky.js')}}"></script> 
    <!-- Bootstrap Validation -->
     
    <script type="text/javascript" src="{{asset('public/js/bootstrapValidator.min.js')}}"></script> 
    <!-- Animations -->
     
    <script type="text/javascript" src="{{asset('public/js/jquery.appear.js')}}"></script> 
    
     
    <!-- Custom Js Code -->
    <script type="text/javascript" src="{{asset('public/js/select2.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/js/custom.js')}}"></script> 
    <script type="text/javascript" src="{{asset('public/AdminPanel/main.js')}}"></script> 
    <!-- Scripts -->
	
	<!-- Include Date Range Picker -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
	<script>
	$(document).ready(function(){
		var date_input=$('input[name="date"]'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		date_input.datepicker({
			format: 'yyyy-mm-dd',
			container: container,
			todayHighlight: true,
			autoclose: true,
		})

        var start_date_input=$('input[name="start_date"]'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		start_date_input.datepicker({
			format: 'yyyy-mm-dd',
			container: container,
			todayHighlight: true,
			autoclose: true,
		})

        var end_date_input=$('input[name="end_date"]'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		end_date_input.datepicker({
			format: 'yyyy-mm-dd',
			container: container,
			todayHighlight: true,
			autoclose: true,
		})
	})
</script>
	
	</body>
</html>
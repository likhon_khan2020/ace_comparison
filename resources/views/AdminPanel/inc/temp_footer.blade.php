<!-- request -->
        <footer id="footer">   
            <!-- footer-bottom -->		
            <div class="copyright">
                <div class="container">
                    <div class="row">
                        <!-- Copyrights -->
                        <div class="col-xs-10 col-sm-6 col-md-6"> &copy; 2021 <a href="https://globalsportsmetrics.com/">globalsportsmetrics.com</a>.
                        <br />
                        <!-- Terms Link -->
                         
                        <!--<a href="#">Terms of Use</a> / 
                        <a href="#">Privacy Policy</a></div>
                        <div class="col-xs-2  col-sm-6 col-md-6 text-right page-scroll gray-bg icons-circle i-3x">                           
                            <a href="#page">
                                <i class="glyphicon glyphicon-arrow-up"></i>
                            </a>
                        </div>-->
						
                    </div>
                </div>
            </div>
            <!-- footer-bottom -->
        </footer>
        <!-- footer -->
    </div>
    <!-- page -->
    <!-- Scripts -->
    <script type="text/javascript" src="{{asset('public/js/jquery.min.js')}}"></script> 
    <script type="text/javascript" src="{{asset('public/js/bootstrap.min.js')}}"></script> 
    <!-- Menu jQuery plugin -->
     
    <script type="text/javascript" src="{{asset('public/js/hover-dropdown-menu.js')}}"></script> 
    <!-- Menu jQuery Bootstrap Addon --> 
    <script type="text/javascript" src="{{asset('public/js/jquery.hover-dropdown-menu-addon.js')}}"></script> 
    <!-- Scroll Top Menu -->
     
    <script type="text/javascript" src="{{asset('public/js/jquery.easing.1.3.js')}}"></script> 
    <!-- Sticky Menu --> 
    <script type="text/javascript" src="{{asset('public/js/jquery.sticky.js')}}"></script> 
    <!-- Bootstrap Validation -->
     
    <script type="text/javascript" src="{{asset('public/js/bootstrapValidator.min.js')}}"></script> 
    <!-- Animations -->
     
    <script type="text/javascript" src="{{asset('public/js/jquery.appear.js')}}"></script> 
    <script type="text/javascript" src="{{asset('public/js/effect.js')}}"></script> 
  
    <!-- Custom Js Code -->
     
    <script type="text/javascript" src="{{asset('public/js/custom.js')}}"></script> 
	<script type="text/javascript" src="{{asset('public/AdminPanel/main.js')}}"></script> 
    <!-- Scripts -->
	
	<!-- Include Date Range Picker -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
	<script>
	$(document).ready(function(){
		var date_input=$('input[name="date"]'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		date_input.datepicker({
			format: 'yyyy/mm/dd',
			container: container,
			todayHighlight: true,
			autoclose: true,
		})
	})
</script>
	
	</body>

</html>
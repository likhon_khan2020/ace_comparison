	@include('UserPanel.inc.header')
    <body style="background-color: lightgray;">
    @include("UserPanel.inc.menu")
        <div class="retest-page-header">
            <div class="container-fluid create-retest-container" style="background-image: url({{url('/')}}/public/img/bg-3.jpg);">
                <h1 class="title text-center" style="color:#fff;">Benchmark Re-Testing</h1>
            </div>
        </div>
        <!-- page-header -->
        <section>
            <div class="container create-retest">
                <div class="row">
                    <div class="content col-sm-12 col-md-8 col-md-offset-2">
                        <form id="testingForm" class="contact-form" method="post" enctype='multipart/form-data' onsubmit="registerReTesting('{{route("registerReTesting")}}'); event.preventDefault();">
						@csrf    
                        <input name="player_id" value="{{$player_id}}" type="hidden"/>                  
                        <div id="success"></div>
                        <div id="error_data" style="padding-bottom:5px;"></div>
						
						<!-- Display Success Message -->
						@if(Session::has('success_msg'))
						@php
						$success_msg = session()->get('success_msg');
						@endphp
						<div class="alert alert-success alert-dismissible " role="alert">
						  <strong>{{$success_msg}}</strong>
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						  </button>
						</div>
						@endif
					
						<div class="row" role="form" style="margin-bottom:15px;">
                            <div class="col-md-4">
                                <span style="font-weight:bold;">Station</span>
                            </div>
                            <div class="col-md-8">
                                <span style="font-weight:bold;">Retest</span>
                            </div>
                        </div>
						
                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                10 yd Sprint							   
                            </div>
                            <div class="col-md-8">
                                <input name="10_yd_sprint" id="10_yd_sprint" type="text" class="form-control"  placeholder="sec" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['10_yd_sprint']}}" readonly @endif />
                            </div>
                        </div>						
						
                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                40 yd Sprint							   
                            </div>
                            <div class="col-md-8">
                                <input name="40_yd_sprint" id="40_yd_sprint" type="text" class="form-control"  placeholder="40 yd Sprint" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['40_yd_sprint']}}" readonly @endif/>
                            </div>
                        </div>
                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                60 yd Sprint							   
                            </div>
                            <div class="col-md-8">
                                <input name="60_yd_sprint" id="60_yd_sprint" type="text" class="form-control"  placeholder="60 yd Sprint" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['60_yd_sprint']}}" readonly @endif />
                            </div>
                        </div>

                        <div class="row" role="form">
                            <div class="col-md-4">                               
                            Grip Strength							   
                            </div>
                            <div class="col-md-8">
                                <input name="grip_strength" id="grip_strength" type="text" class="form-control"  placeholder="Grip Strength" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['grip_strength']}}" readonly @endif />
                            </div>
                        </div>
                       
                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                Broad Jump							   
                            </div>
                            <div class="col-md-8">
                                <input name="broad_jump" id="broad_jump" type="text" class="form-control"  placeholder="Broad Jump" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['broad_jump']}}" readonly @endif />
                            </div>
                        </div>

                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                Vertical Jump							   
                            </div>
                            <div class="col-md-8">
                                <input name="vertical_jump" id="vertical_jump" type="text" class="form-control"  placeholder="Vertical Jump" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['vertical_jump']}}" readonly @endif  />
                            </div>
                        </div>

                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                Shoulder Flex							   
                            </div>
                            <div class="col-md-8">
                                <input name="ss_shoulder_flex" id="ss_shoulder_flex" type="text" class="form-control"  placeholder="Shoulder Flex" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['ss_shoulder_flex']}}" readonly @endif />
                            </div>
                        </div>
                   
                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                Pro Agility							   
                            </div>
                            <div class="col-md-8">
                                <input name="pro_agility" id="pro_agility" type="text" class="form-control"  placeholder="Pro Agility" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['pro_agility']}}" readonly @endif />
                            </div>
                        </div>

                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                Throwing Velocity							   
                            </div>
                            <div class="col-md-8">
                                <input name="throwing_velocity" id="throwing_velocity" type="text" class="form-control"  placeholder="Throwing Velocity" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['throwing_velocity']}}" readonly @endif />
                            </div>
                        </div>

                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                Exit Velocity							   
                            </div>
                            <div class="col-md-8">
                                <input name="exit_velocity" id="exit_velocity" type="text" class="form-control"  placeholder="Exit Velocity" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['exit_velocity']}}" readonly @endif />
                            </div>
                        </div>
                                                
                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                Stand And Reach							   
                            </div>
                            <div class="col-md-8">
                                <input name="stand_and_reach" id="stand_and_reach" type="text" class="form-control"  placeholder="Stand And Reach" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['stand_and_reach']}}" readonly @endif />
                            </div>
                        </div>

                        <div class="row" role="form">
                            <div class="col-md-4">                               
                                HTPS						   
                            </div>
                            <div class="col-md-8">
                                <input name="htps" id="htps" type="text" class="form-control"  placeholder="HTPS" @if(isset($benchmarkInfo)) value="{{$benchmarkInfo['htps']}}" readonly @endif />
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <button id="submit" class="btn btn-default custom-btn">Submit</button> 
                        <a class="btn btn-primary custom-btn" href="{{url('/profile')}}" role="button">Goto Profile</a>
                        </form>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- page-section -->
        
        @include('UserPanel/inc/footer')

<!DOCTYPE html>
<html lang="en">
    
<head>
        <meta charset="utf-8" />
        <title>Athletic Capacity Evaluation</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="keywords" content="HTML5 Template" />
        <meta name="description" content="Player Record HTML Template" />
        <meta name="author" content="playerrecord.com" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!-- Favicon -->
        <link rel="shortcut icon" href="{{asset('public/img/favicon.ico')}}" />
        <!-- Font -->
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Arimo:300,400,700,400italic,700italic' />
        <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css' />
        <!-- Font Awesome Icons -->
        <link href="{{asset('public/css/font-awesome.min.css')}}" rel='stylesheet' type='text/css' />
        <!-- Bootstrap core CSS -->
        <link href="{{asset('public/css/bootstrap.min.css')}}" rel="stylesheet" />
        <link href="{{asset('public/css/hover-dropdown-menu.css')}}" rel="stylesheet" />
        <!-- Icomoon Icons -->
        <link href="{{asset('public/css/icons.css')}}" rel="stylesheet" />
        <!-- Revolution Slider -->
        <link href="{{asset('public/css/revolution-slider.css')}}" rel="stylesheet" />
        <link href="{{asset('public/rs-plugin/css/settings.css')}}" rel="stylesheet" />
        <!-- Animations -->
        <link href="{{asset('public/css/animate.min.css')}}" rel="stylesheet" />

        <!-- Owl Carousel Slider -->
        <link href="{{asset('public/css/owl/owl.carousel.css')}}" rel="stylesheet" />
        <link href="{{asset('public/css/owl/owl.theme.css')}}" rel="stylesheet" />
        <link href="{{asset('public/css/owl/owl.transitions.css')}}" rel="stylesheet" />
        <!-- PrettyPhoto Popup -->
        <link href="{{asset('public/css/prettyPhoto.css')}}" rel="stylesheet" />
        <!-- Custom Style -->
        <link href="{{asset('public/css/style.css')}}" rel="stylesheet" />
       <link href="{{asset('public/css/responsive.css')}}" rel="stylesheet" />
        <!-- Color Scheme -->
        <link href="{{asset('public/css/color.css')}}" rel="stylesheet" />
		
        <link href="{{asset('public/AdminPanel/custom.css')}}" rel="stylesheet" />

        <style>
            .contact-form{
            position: absolute;
            top: 50%;
            width: 350px;
            left: 50%;
            border-radius:6px;
            box-shadow:#333 0px 8px 24px;
            transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            -webkit-transform: translate(-50%, -50%);
            }
        </style>
    </head>
    <body style="background-image:url(public/img/bg1.jpg);background-size:cover;">

	    <!-- form-section -->
        <form id="loginForm" class="light-bg contact-form pad-40-custom" method="post" onsubmit="adminLogin('{{route("adminLogin")}}'); event.preventDefault();">
            @csrf
            <h3 class="title" style="text-align:center">Admin Login</h3>

            <!-- Display Email Success Message -->
            @if(Session::has('email_confirmation'))
            @php
            $email_confirmation_msg = session()->get('email_confirmation');
            @endphp
            <div style="color:green;">
                <strong style="position: relative;bottom: 10px;">{{$email_confirmation_msg}}</strong>
            </div>
            @endif

            <!-- Success Message -->
            @if(Session::has('success_msg'))
            @php
            $success_msg = session()->get('success_msg');
            @endphp
            <div style="color:green;">
                <strong style="position: relative;bottom: 10px;">{{$success_msg}}</strong>
            </div>
            @endif


            <div id="login_success"></div>
            <div id="login_error_data" style="padding-bottom:5px;"></div>
            <input class="form-control" type="text" name="email" id="login_email" placeholder="Email *" /> 
            <input class="form-control" type="password" name="password" id="login_password" placeholder="Password *" />
            <div class="clearfix"></div>
            <button id="submit" class="btn btn-default">Submit</button> 
            <span class="pull-right">
                <a href="#" data-toggle="modal" data-target="#passwordResetModal" class="text-success" style="color:red">Forgot Password?</a>
            </span> 
        </form>
        <!-- form-section -->

        <div class="modal" id="passwordResetModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <form id="passwordResetForm">
                    <input id="password_reset_token" name="_token" value="{{csrf_token()}}" type="hidden" />
                    <div class="modal-content">
                    <div class="modal-header">
                        <span class="modal-title">Password Reset</span>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="error_data" style="padding-bottom:5px;"></div>
                        <input name="email" id="email" type="text" placeholder="Enter Your Mail" class="form-control" />
                    </div>
                    <div class="modal-footer">
                        <button onclick="passwordResetEmail(event.preventDefault())" type="button" class="btn btn-primary">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    </div>
                </form>
            </div>
        </div>
        
    <!-- Scripts -->
    <script type="text/javascript" src="{{asset('public/js/jquery.min.js')}}"></script> 
    <script type="text/javascript" src="{{asset('public/js/bootstrap.min.js')}}"></script> 
    <!-- Menu jQuery plugin -->
     
    <script type="text/javascript" src="{{asset('public/js/hover-dropdown-menu.js')}}"></script> 
    <!-- Menu jQuery Bootstrap Addon --> 
    <script type="text/javascript" src="{{asset('public/js/jquery.hover-dropdown-menu-addon.js')}}"></script> 
    <!-- Scroll Top Menu -->
     
    <script type="text/javascript" src="{{asset('public/js/jquery.easing.1.3.js')}}"></script> 
    <!-- Sticky Menu --> 
    <script type="text/javascript" src="{{asset('public/js/jquery.sticky.js')}}"></script> 
    <!-- Bootstrap Validation -->
     
    <script type="text/javascript" src="{{asset('public/js/bootstrapValidator.min.js')}}"></script> 
    <!-- Animations -->
     
    <script type="text/javascript" src="{{asset('public/js/jquery.appear.js')}}"></script> 
    <script type="text/javascript" src="{{asset('public/js/effect.js')}}"></script> 
  
    <!-- Custom Js Code -->
     
    <script type="text/javascript" src="{{asset('public/js/custom.js')}}"></script> 
	<script type="text/javascript" src="{{asset('public/AdminPanel/main.js')}}"></script> 
    <!-- Scripts -->
	
	<!-- Include Date Range Picker -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
	<script>
	$(document).ready(function(){
		var date_input=$('input[name="date"]'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		date_input.datepicker({
			format: 'yyyy/mm/dd',
			container: container,
			todayHighlight: true,
			autoclose: true,
		})
	})
</script>
	
	</body>

</html>

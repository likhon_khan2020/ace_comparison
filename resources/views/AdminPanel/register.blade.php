	
    @include('AdminPanel.inc.header')
    <body>
    @include("AdminPanel.inc.menu")

        <div class="retest-page-header">
            <div class="container-fluid create-retest-container" style="background-image: url({{url('/')}}/public/img/bg-3.jpg);">
                <h1 class="title text-center" id="title" style="color:#fff;">@if(isset($editData)) Update Profile @else Player Registration @endif</h1>
            </div>
        </div>
      
        <section class="page-section">
            <div class="container">
                <div class="row">
                    <div class="content col-sm-12 col-md-8 col-md-offset-2">
                        <form id="registrationForm" class="contact-form" method="post" enctype='multipart/form-data' @if(isset($editData)) onsubmit="editPlayer('{{route("editPlayer")}}'); event.preventDefault();" @else onsubmit="registerPlayer('{{route("admin.registerPlayer")}}'); event.preventDefault();" @endif>
						@csrf
                        <input name="player_id" value="@if(isset($editData)){{$editData['0']->id}}@endif" type="hidden" />
                   
                        <div id="success"></div>
                        <div id="error_data" style="padding-bottom:5px;"></div>
						
						<!-- Display Success Message -->
						@if(Session::has('success_msg'))
						@php
						$success_msg = session()->get('success_msg');
						@endphp
						<div class="alert alert-success alert-dismissible " role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						  </button>
						  <strong>{!! $success_msg !!}</strong>						  
						</div>
						@endif
						<span style="display:block;">User Name</span>
                        <input class="form-control" type="text" name="user_name" id="user_name" value="@if(isset($editData)){{$editData['0']->user_name}}@endif" placeholder="User Name *" />
                       
                        <span style="display:block;">Email</span> 
                        <input class="form-control" type="email" name="email" id="email" value="@if(isset($editData)){{$editData['0']->email}}@endif" placeholder="Email *" />
						
                        <div class="row" role="form">
                            <div class="col-md-6">
                                <span style="display:block;">Password</span>
                                <input name="password" id="password" type="password" class="form-control"  placeholder="Password @if(!isset($editData)) * @endif" />
                            </div>
                            <div class="col-md-6">
                                <span style="display:block;">Confirm Password</span>
                                <input name="con_password" id="con_password" type="password" class="form-control"  placeholder="Re-Enter Password @if(!isset($editData)) * @endif" />
                            </div>
                        </div>

                        <span style="display:block;">First Name</span>
						<input class="form-control" type="text" name="first_name" id="first_name" value="@if(isset($editData)){{$editData['0']->first_name}}@endif" placeholder="First Name *" /> 
						
                        <span style="display:block;">Last Name</span>
                        <input class="form-control" type="text" name="last_name" id="last_name" value="@if(isset($editData)){{$editData['0']->last_name}}@endif" placeholder="Last Name *" /> 

                        <span style="display:block;">Address</span>
                        <input class="form-control" type="text" name="address" id="address" value="@if(isset($editData)){{$editData['0']->address}}@endif" placeholder="Address *" /> 
                        
                        <div class="row" role="form">
                            <div class="col-md-7 col-sm-12 col-xs-12" style="height:85px;">
                                <!--<div class="input-group">
									<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
									<input class="form-control" id="date" name="date" placeholder="Date of Birth * (MM/DD/YYYY)" type="text"/>
							    </div>-->
                                <span style="display:block;">Date of Birth</span>
                                <select name="day" id="day" class="form-select form-control" style="width:30%; float: left;" aria-label="Default select example">
								  <option value="" selected>Select Day</option>
								  @for($day = 1; $day <= 31;$day++)
								  <option value="{{$day}}" @if(isset($editData) && substr($editData['0']->birthday,8,2) == $day) selected @endif>{{$day}}</option>
                                  @endfor
								</select>
                                <select name="month" id="month" class="form-select form-control" style="width:30%; float: left;margin-left: 18px;" aria-label="Default select example">
								  <option value="" selected>Select Month</option>
								  <option value="01" @if(isset($editData) && substr($editData['0']->birthday,5,2) == '01') selected @endif>Jan</option>
                                  <option value="02" @if(isset($editData) && substr($editData['0']->birthday,5,2) == '02') selected @endif>Feb</option>
                                  <option value="03" @if(isset($editData) && substr($editData['0']->birthday,5,2) == '03') selected @endif>Mar</option>
                                  <option value="04" @if(isset($editData) && substr($editData['0']->birthday,5,2) == '04') selected @endif>Apr</option>
                                  <option value="05" @if(isset($editData) && substr($editData['0']->birthday,5,2) == '05') selected @endif>May</option>
                                  <option value="06" @if(isset($editData) && substr($editData['0']->birthday,5,2) == '06') selected @endif>Jun</option>
                                  <option value="07" @if(isset($editData) && substr($editData['0']->birthday,5,2) == '07') selected @endif>Jul</option>
                                  <option value="08" @if(isset($editData) && substr($editData['0']->birthday,5,2) == '08') selected @endif>Aug</option>
                                  <option value="09" @if(isset($editData) && substr($editData['0']->birthday,5,2) == '09') selected @endif>Sep</option>
                                  <option value="10" @if(isset($editData) && substr($editData['0']->birthday,5,2) == '10') selected @endif>Oct</option>
                                  <option value="11" @if(isset($editData) && substr($editData['0']->birthday,5,2) == '11') selected @endif>Nov</option>
                                  <option value="12" @if(isset($editData) && substr($editData['0']->birthday,5,2) == '12') selected @endif>Dec</option>
								</select>
                                <select name="year" id="year" class="form-select form-control" style="width:30%;float: left;margin-left: 18px;" aria-label="Default select example">
								  <option value="" selected>Select Year</option>
                                  @for($i = 2021; $i > 1964;$i--)
								  <option value="{{$i}}" @if(isset($editData) && substr($editData['0']->birthday,0,4) == $i) selected @endif>{{$i}}</option>
                                  @endfor
								</select>
                            </div>
                            <div class="col-md-5 col-sm-12 col-xs-12">
                                <span style="display:block;">Sport</span>
                                <select name="sport" id="sport" class="form-select form-control" aria-label="Default select example">
								  <option value="" selected>Select Sport</option>
								  <option value="Baseball" @if(isset($editData) && $editData['0']->sport == 'Baseball') selected @endif>Baseball</option>
								</select>
                            </div>
                        </div>	

						<div class="row" role="form">
                            <div class="col-md-6">
                                <span style="display:block;">Height</span>
                                <input name="height" id="height" value="@if(isset($editData)){{$editData['0']->height}}@endif" type="text" class="form-control"  placeholder="Height" />
                            </div>
                            <div class="col-md-6">
                                <span style="display:block;">Weight</span>
                                <input name="weight" id="weight" value="@if(isset($editData)){{$editData['0']->weight}}@endif" 
                                type="text" class="form-control"  placeholder="Weight"
                                style="width:52%;float:left;" />
                                <select name="weight_unit" id="weight_unit" class="form-select form-control" aria-label="Default select example" style="width:45%;position: relative;left: 10px;">
                                    <option value="" selected>Select Unit</option>
                                    <option value="lbs">lbs</option>
                                    <option value="kg">kg</option>
                                    </select>
                            </div>
                        </div>                       

                        <div class="row" role="form">
                            <div class="col-md-6">
                                <span style="display:block;">Level</span>
                                <select name="level" id="level" class="form-select form-control" aria-label="Default select example">
                                <option value="" selected>Select Level</option>
                                <option value="1" @if(isset($editData) && $editData['0']->level == '1') selected @endif>Professional</option>
                                <option value="2" @if(isset($editData) && $editData['0']->level == '2') selected @endif>Division 1</option>
                                <option value="3" @if(isset($editData) && $editData['0']->level == '3') selected @endif>Division 2/ NAIA</option>
                                <option value="4" @if(isset($editData) && $editData['0']->level == '4') selected @endif>Jr. College</option>
                                <option value="5" @if(isset($editData) && $editData['0']->level == '5') selected @endif>HS Varsity</option>
                                <option value="6" @if(isset($editData) && $editData['0']->level == '6') selected @endif>16U Athlete</option>
                                <option value="7" @if(isset($editData) && $editData['0']->level == '7') selected @endif>15U Athlete</option>
                                <option value="8" @if(isset($editData) && $editData['0']->level == '8') selected @endif>14U Athlete</option>
                                <option value="9" @if(isset($editData) && $editData['0']->level == '9') selected @endif>13U Athlete</option>
                                <option value="10" @if(isset($editData) && $editData['0']->level == '10') selected @endif>12U Athlete</option>
                                <option value="11" @if(isset($editData) && $editData['0']->level == '11') selected @endif>11U Athlete</option>
                                <option value="12" @if(isset($editData) && $editData['0']->level == '12') selected @endif>10U Athlete</option>
                                <option value="13" @if(isset($editData) && $editData['0']->level == '13') selected @endif>9U Athlete</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <span style="display:block;">Gender</span>
                                <select name="gender" id="gender" class="form-select form-control" aria-label="Default select example">
                                    <option value="" selected>Select Gender</option>
                                    <option value="Male" @if(isset($editData) && $editData['0']->gender == 'Male') selected @endif>Male</option>
                                    <option value="Female" @if(isset($editData) && $editData['0']->gender == 'Female') selected @endif>Female</option>                            
                                </select>
                            </div>
                        </div>

						<div class="row" role="form">
                            <div class="col-md-6">
                                <span style="display:block;">Phone</span>
                                <input name="phone" id="phone" value="@if(isset($editData)){{$editData['0']->phone}}@endif" 
                                type="text" class="form-control"  placeholder="Phone *" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1').replace(/\./g, '');" />
                            </div>
                            <div class="col-md-6">
                                <span style="display:block;">Graduation Year</span>
                                <input class="form-control" type="text" name="graduation_year" id="graduation_year" 
                                value="@if(isset($editData)){{$editData['0']->graduation_year}}@endif"
                                 placeholder="Graduation Year" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1').replace(/\./g, '');" />
                            </div>
                        </div>
						
                        <span style="display:block;">Profile Pic</span>	
                        <input name="old_filename" value="@if(isset($editData)){{$editData['0']->filename}}@endif" type="hidden"/>						
						<input name="profile_pic" class="form-control form-control-lg" id="profile_pic" type="file" accept=".jpg" />
                        <div class="clearfix"></div>
                        <button id="submit" class="btn btn-default" style="background: green;color: #fff;">@if(isset($editData)) Update @else Register Now @endif</button> 
                        <a class="btn btn-primary-custom" href="{{url('/adminProfile')}}" role="button">Go Back</a>
                        <div class="loader" style="display:none"></div>
                        <!-- .buttons-box --></form>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- page-section -->
        
    @include('AdminPanel.inc.footer')

@if ($playerList->hasPages())
    <ul class="pagination justify-content-start float-end">
        @if ($playerList->onFirstPage())
            <li class="page-item disabled"><a class="page-link" ><i class="fa fa-angle-double-left"></i></a></li>
        @else
            <li class="page-item page-item"><a class="page-link" href="{{ $playerList->previousPageUrl() }}" rel="prev"><i class="fa fa-angle-double-left"></i></a></li>
        @endif

        @if($playerList->currentPage() > 3)
            <li class="page-item hidden-xs"><a class="page-link" href="{{ $playerList->url(1) }}">1</a></li>
        @endif
        @if($playerList->currentPage() > 4)
            <li><span style="margin-left:5px !important;padding: 0px 2px;">...</span></li>
        @endif
        @foreach(range(1, $playerList->lastPage()) as $i)
            @if($i >= $playerList->currentPage() - 2 && $i <= $playerList->currentPage() + 2)
                @if ($i == $playerList->currentPage())
                    <li class="page-item active"><a class="page-link" >{{ $i }}</a></li>
                @else
                <li class="page-item"><a class="page-link" href="{{ $playerList->url($i) }}">{{ $i }}</a></li>
                @endif
            @endif
        @endforeach
        @if($playerList->currentPage() < $playerList->lastPage() - 3)
        <li class="page-item"><span style="margin-left:5px !important;padding: 0px 2px;">...</span></li>
        @endif
        @if($playerList->currentPage() < $playerList->lastPage() - 2)
            <li class="page-item hidden-xs"><a class="page-link" href="{{ $playerList->url($playerList->lastPage()) }}">{{ $playerList->lastPage() }}</a></li>
        @endif

        @if ($playerList->hasMorePages())
        <li class="page-item"><a class="page-link" href="{{ $playerList->nextPageUrl() }}" rel="next"><i class="fa fa-angle-double-right"></i></a></li>
        @else
            <li class="disabled"><a class="page-link" ><i class="fa fa-angle-double-right"></i></a></li>
        @endif
    </ul>
@endif
@include('AdminPanel.inc.header')
<body style="background-color: lightgray;">
<section>

	@include("AdminPanel.inc.menu")

	<div id="print">
	<style>	
		@media print {
			body {-webkit-print-color-adjust: exact;}
			.print-hidden{
				display:none !important;
			}			
			.profile-edit-button{
				display:none !important;
			}
			.view-admin-btn{
				display:none !important;
			}
			.add-admin-btn{
				display:none !important;
			}
			.view-player-btn{
				display:none !important;
			}
			.add-player-btn{
				display:none !important;
			}
			.compare-retest-main{
				page-break-before: always;
			}
			.profile-main{
				box-shadow:none;			
			}			
			.profile-img img{
				position:relative;
				left:35%;
				min-width:280px !important;
				min-height:280px !important;
			}	
			.active-status{
				margin-top:10px;
				font-size:20px;
			}
			.user-mini-part{
				width:100%;
				margin-top:70px;
				margin-left:18%;
			}	
			.user-mini-part h3{
				font-size:25px;	
			}	
			.user-mini-part span{
				font-size:22px;	
			}		
			.profile-table{
				width:100%;
				margin-left:18%;
				margin-top:20px;
			}			
			.profile-table>tbody>tr>th {
				font-size:20px;		
				text-align:left;
			}
			.profile-table>tbody>tr>td {		
				font-size:20px;
			}			
			.compare-retest-main{
				box-shadow:none;
			}
			.compare-retest-title{
				font-size:27px;
			}
			.dashboard-header{
				width:100%;
			}

			.dashboard-header {
				font-size:18px;
			}
					
			.dashboard-table{
				width:100%;
				margin-top:15px;
			}
		}
	</style>

	@include("AdminPanel.inc.profile_header")

		<div class="container compare-retest-main">
			<div class="row">
				<div class="col-sm-12 col-md-12" style="color:#fff;">	
					<div style="color:#000;text-align: center;margin-top:15px;margin-bottom:15px;">
						<h3 class="compare-retest-title">Compare Retest</h3>
						<span onclick="printCompareRetest()" style="position: absolute;right: 20px;top: 20px;font-size: 25px;"><i class="fa fa-print"></i></span>
					</div>		
					<div class="table-responsive">
						<table class="table table-bordered dashboard-header">
						<tbody>
						<tr>
							<td style="font-weight:bold;text-align:center">Test Taken By: <span style="font-weight: 100;">@if(isset($retestInfo)){{$retestInfo['first_name'].' '.$retestInfo['last_name']}}@endif </span></td>
							<td style="font-weight:bold;text-align:center">Level</td>
							<td style="text-align:center">@if(isset($retestInfo)){{$retestInfo['level_name']}}@endif</td>
							<td></td>
						</tr>
						<tr>
							<td style="font-weight:bold;text-align:center">Benchmark Testing Date</td>
							<td style="text-align:center">{{substr($benchmarkInfo['inserted_date'],0,10)}}</td>
							<td style="font-weight:bold;text-align:center">Improved</td>
							<td style="width:12%;background:#95D195"></td>
						</tr>
						<tr>
							<td style="font-weight:bold;text-align:center">Re-Testing Date</td>
							<td style="text-align:center">{{substr($retestInfo['inserted_date'],0,10)}}</td>
							<td style="font-weight:bold;text-align:center">Stayed The Same</td>
							<td style="width:12%;background:#FAFB57"></td>
						</tr>
						<tr>
							@php
							$earlier = new DateTime(substr($benchmarkInfo['inserted_date'],0,10));
							$later = new DateTime(substr($retestInfo['inserted_date'],0,10));
							$pos_diff = $earlier->diff($later)->format("%r%a"); 
							@endphp
							<td style="font-weight:bold;text-align:center"># Of Days Between Testing</td>
							<td style="text-align:center">{{$pos_diff}}</td>
							<td style="font-weight:bold;text-align:center">Declined</td>
							<td style="width:12%;background:#F65E5D"></td>
						</tr>
						
						</tbody>
					</table>
					</div>		
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-12" style="color:#fff;">	
					<div class="table-responsive">
						<table class="table table-striped dashboard-table">
							<thead>
								<tr>
								<th style="background: lightgray;text-align:center">Station</th>
								<th style="background: lightgray;text-align:center">Benchmark Testing</th>
								<th style="background: lightgray;text-align:center">Re-Testing</th>
								<th style="background: cornflowerblue;text-align:center">Improvement</th>
								<th style="background: lightgray;">Goal</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="text-align:center">10 yd Sprint (sec)</td>
									<td style="text-align:center">{{$benchmarkInfo['10_yd_sprint']}}</td>
									<td style="text-align:center">{{$retestInfo['10_yd_sprint']}}</td>
									<td @if($retestInfo['10_yd_sprint'] == 0) style={{'background:#FAFB57;text-align:center'}} @elseif($retestInfo['10_yd_sprint'] < $benchmarkInfo['10_yd_sprint']) style={{'background:#95D195;text-align:center'}} @elseif($retestInfo['10_yd_sprint'] > $benchmarkInfo['10_yd_sprint'] ) style={{'background:#F65E5D;text-align:center'}} @elseif(($retestInfo['10_yd_sprint'] == $benchmarkInfo['10_yd_sprint'])) style={{'background:#FAFB57;text-align:center'}}  @endif>									
										@if($retestInfo['10_yd_sprint'] == 0) 
										{{$benchmarkInfo['10_yd_sprint']}}
										@else
										{{$retestInfo['10_yd_sprint'] - $benchmarkInfo['10_yd_sprint']}}
										@endif
									</td>
									<td class="goal-td-bg" style="text-align:center">@if(isset($goalInfo['10_yd_sprint'])){{$goalInfo['10_yd_sprint']}}@endif</td>
								</tr>
								
								<tr>
									<td style="text-align:center">40 yd Sprint (sec)</td>
									<td style="text-align:center">{{$benchmarkInfo['40_yd_sprint']}}</td>
									<td style="text-align:center">{{$retestInfo['40_yd_sprint']}}</td>
									<td @if($retestInfo['40_yd_sprint'] == 0) style={{'background:#FAFB57;text-align:center'}} @elseif($retestInfo['40_yd_sprint'] < $benchmarkInfo['40_yd_sprint']) style={{'background:#95D195;text-align:center'}} @elseif($retestInfo['40_yd_sprint'] > $benchmarkInfo['40_yd_sprint'] ) style={{'background:#F65E5D;text-align:center'}} @elseif(($retestInfo['40_yd_sprint'] == $benchmarkInfo['40_yd_sprint'])) style={{'background:#FAFB57;text-align:center'}}  @endif>								
										@if($retestInfo['40_yd_sprint'] == 0) 
										{{$benchmarkInfo['40_yd_sprint']}}
										@else
										{{$retestInfo['40_yd_sprint'] - $benchmarkInfo['40_yd_sprint']}}
										@endif
									</td>
									<td class="goal-td-bg" style="text-align:center">@if(isset($goalInfo['40_yd_sprint'])){{$goalInfo['40_yd_sprint']}}@endif</td>
								</tr>
								<tr>
									<td style="text-align:center">60 yd Sprint (sec)</td>
									<td style="text-align:center">{{$benchmarkInfo['60_yd_sprint']}}</td>
									<td style="text-align:center">{{$retestInfo['60_yd_sprint']}}</td>
									<td @if($retestInfo['60_yd_sprint'] == 0) style={{'background:#FAFB57;text-align:center'}} @elseif($retestInfo['60_yd_sprint'] < $benchmarkInfo['60_yd_sprint']) style={{'background:#95D195;text-align:center'}} @elseif($retestInfo['60_yd_sprint'] > $benchmarkInfo['60_yd_sprint'] ) style={{'background:#F65E5D;text-align:center'}} @elseif(($retestInfo['60_yd_sprint'] == $benchmarkInfo['60_yd_sprint'])) style={{'background:#FAFB57;text-align:center'}}  @endif>								
										@if($retestInfo['60_yd_sprint'] == 0) 
										{{$benchmarkInfo['60_yd_sprint']}}
										@else
										{{$retestInfo['60_yd_sprint'] - $benchmarkInfo['60_yd_sprint']}}
										@endif
									</td>
									<td class="goal-td-bg" style="text-align:center">@if(isset($goalInfo['60_yd_sprint'])){{$goalInfo['60_yd_sprint']}}@endif</td>
								</tr>

								<tr>
									<td style="text-align:center">Grip Strength (lbs)</td>
									<td style="text-align:center">{{$benchmarkInfo['grip_strength']}}</td>
									<td style="text-align:center">{{$retestInfo['grip_strength']}}</td>
									<td @if($retestInfo['grip_strength'] == 0) style={{'background:#FAFB57;text-align:center'}} @elseif($retestInfo['grip_strength'] - $benchmarkInfo['grip_strength'] < 0) style={{'background:#F65E5D;text-align:center'}} @elseif(($retestInfo['grip_strength'] - $benchmarkInfo['grip_strength']) == 0) style={{'background:#FAFB57;text-align:center'}} @else style={{'background:#95D195;text-align:center'}} @endif>
										@if($retestInfo['grip_strength'] == 0) 
										{{$benchmarkInfo['grip_strength']}}
										@else
										{{$retestInfo['grip_strength'] - $benchmarkInfo['grip_strength']}}
										@endif
									</td>
									<td class="goal-td-bg" style="text-align:center">@if(isset($goalInfo['grip_strength'])){{$goalInfo['grip_strength']}}@endif</td>
								</tr>
								
								<tr>
									<td style="text-align:center">Broad Jump (in)</td>
									<td style="text-align:center">{{$benchmarkInfo['broad_jump']}}</td>
									<td style="text-align:center">{{$retestInfo['broad_jump']}}</td>
									<td @if($retestInfo['broad_jump'] == 0) style={{'background:#FAFB57;text-align:center'}} @elseif($retestInfo['broad_jump'] - $benchmarkInfo['broad_jump'] < 0) style={{'background:#F65E5D;text-align:center'}} @elseif(($retestInfo['broad_jump'] - $benchmarkInfo['broad_jump']) == 0) style={{'background:#FAFB57;text-align:center'}} @else style={{'background:#95D195;text-align:center'}} @endif>
										@if($retestInfo['broad_jump'] == 0) 
										{{$benchmarkInfo['broad_jump']}}
										@else
										{{$retestInfo['broad_jump'] - $benchmarkInfo['broad_jump']}}
										@endif
									</td>
									<td class="goal-td-bg" style="text-align:center">@if(isset($goalInfo['broad_jump'])){{$goalInfo['broad_jump']}}@endif</td>
								</tr>

								<tr>
									<td style="text-align:center">Vertical Jump (in)</td>
									<td style="text-align:center">{{$benchmarkInfo['vertical_jump']}}</td>
									<td style="text-align:center">{{$retestInfo['vertical_jump']}}</td>
									<td @if($retestInfo['vertical_jump'] == 0) style={{'background:#FAFB57;text-align:center'}} @elseif($retestInfo['vertical_jump'] - $benchmarkInfo['vertical_jump'] < 0) style={{'background:#F65E5D;text-align:center'}} @elseif(($retestInfo['vertical_jump'] - $benchmarkInfo['vertical_jump']) == 0) style={{'background:#FAFB57;text-align:center'}} @else style={{'background:#95D195;text-align:center'}} @endif>
										@if($retestInfo['vertical_jump'] == 0) 
										{{$benchmarkInfo['vertical_jump']}}
										@else
										{{$retestInfo['vertical_jump'] - $benchmarkInfo['vertical_jump']}}
										@endif
									</td>
									<td class="goal-td-bg" style="text-align:center">@if(isset($goalInfo['vertical_jump'])){{$goalInfo['vertical_jump']}}@endif</td>
								</tr>

								<tr>
									<td style="text-align:center">Shoulder Flex (in)</td>
									<td style="text-align:center">{{$benchmarkInfo['ss_shoulder_flex']}}</td>
									<td style="text-align:center">{{$retestInfo['ss_shoulder_flex']}}</td>
									<td @if($retestInfo['ss_shoulder_flex'] == 0) style={{'background:#FAFB57;text-align:center'}} @elseif($retestInfo['ss_shoulder_flex'] < $benchmarkInfo['ss_shoulder_flex']) style={{'background:#95D195;text-align:center'}} @elseif($retestInfo['ss_shoulder_flex'] > $benchmarkInfo['ss_shoulder_flex'] ) style={{'background:#F65E5D;text-align:center'}} @elseif(($retestInfo['ss_shoulder_flex'] == $benchmarkInfo['ss_shoulder_flex'])) style={{'background:#FAFB57;text-align:center'}}  @endif>							
										@if($retestInfo['ss_shoulder_flex'] == 0) 
										{{$benchmarkInfo['ss_shoulder_flex']}}
										@else
										{{$retestInfo['ss_shoulder_flex'] - $benchmarkInfo['ss_shoulder_flex']}}
										@endif
									</td>
									<td class="goal-td-bg" style="text-align:center">@if(isset($goalInfo['ss_shoulder_flex'])){{$goalInfo['ss_shoulder_flex']}}@endif</td>
								</tr>
								
								<tr>
									<td style="text-align:center">Pro Agility (sec)</td>
									<td style="text-align:center">{{$benchmarkInfo['pro_agility']}}</td>
									<td style="text-align:center">{{$retestInfo['pro_agility']}}</td>
									<td @if($retestInfo['pro_agility'] == 0) style={{'background:#FAFB57;text-align:center'}} @elseif($retestInfo['pro_agility'] < $benchmarkInfo['pro_agility']) style={{'background:#95D195;text-align:center'}} @elseif($retestInfo['pro_agility'] > $benchmarkInfo['pro_agility'] ) style={{'background:#F65E5D;text-align:center'}} @elseif(($retestInfo['pro_agility'] == $benchmarkInfo['pro_agility'])) style={{'background:#FAFB57;text-align:center'}}  @endif>
										@if($retestInfo['pro_agility'] == 0) 
										{{$benchmarkInfo['pro_agility']}}
										@else
										{{$retestInfo['pro_agility'] - $benchmarkInfo['pro_agility']}}
										@endif
									</td>
									<td class="goal-td-bg" style="text-align:center">@if(isset($goalInfo['pro_agility'])){{$goalInfo['pro_agility']}}@endif</td>
								</tr>

								<tr>
									<td style="text-align:center">Throwing Velocity (mph)</td>
									<td style="text-align:center">{{$benchmarkInfo['throwing_velocity']}}</td>
									<td style="text-align:center">{{$retestInfo['throwing_velocity']}}</td>
									<td @if($retestInfo['throwing_velocity'] == 0) style={{'background:#FAFB57;text-align:center'}} @elseif($retestInfo['throwing_velocity'] - $benchmarkInfo['throwing_velocity'] < 0) style={{'background:#F65E5D;text-align:center'}} @elseif(($retestInfo['throwing_velocity'] - $benchmarkInfo['throwing_velocity']) == 0) style={{'background:#FAFB57;text-align:center'}} @else style={{'background:#95D195;text-align:center'}} @endif>
										@if($retestInfo['throwing_velocity'] == 0) 
										{{$benchmarkInfo['throwing_velocity']}}
										@else
										{{$retestInfo['throwing_velocity'] - $benchmarkInfo['throwing_velocity']}}
										@endif
									</td>
									<td class="goal-td-bg" style="text-align:center">@if(isset($goalInfo['throwing_velocity'])){{$goalInfo['throwing_velocity']}}@endif</td>
								</tr>

								<tr>
									<td style="text-align:center">Exit Velocity (mph)</td>
									<td style="text-align:center">{{$benchmarkInfo['exit_velocity']}}</td>
									<td style="text-align:center">{{$retestInfo['exit_velocity']}}</td>
									<td @if($retestInfo['exit_velocity'] == 0) style={{'background:#FAFB57;text-align:center'}} @elseif($retestInfo['exit_velocity'] - $benchmarkInfo['exit_velocity'] < 0) style={{'background:#F65E5D;text-align:center'}} @elseif(($retestInfo['exit_velocity'] - $benchmarkInfo['exit_velocity']) == 0) style={{'background:#FAFB57;text-align:center'}} @else style={{'background:#95D195;text-align:center'}} @endif>
										@if($retestInfo['exit_velocity'] == 0) 
										{{$benchmarkInfo['exit_velocity']}}
										@else
										{{$retestInfo['exit_velocity'] - $benchmarkInfo['exit_velocity']}}
										@endif
									</td>
									<td class="goal-td-bg" style="text-align:center">@if(isset($goalInfo['exit_velocity'])){{$goalInfo['exit_velocity']}}@endif</td>
								</tr>						
							
								<tr>
									<td style="text-align:center">Stand And Reach (in)</td>
									<td style="text-align:center">{{$benchmarkInfo['stand_and_reach']}}</td>
									<td style="text-align:center">{{$retestInfo['stand_and_reach']}}</td>
									<td @if($retestInfo['stand_and_reach'] == 0) style={{'background:#FAFB57;text-align:center'}} @elseif($retestInfo['stand_and_reach'] < $benchmarkInfo['stand_and_reach']) style={{'background:#95D195;text-align:center'}} @elseif($retestInfo['stand_and_reach'] > $benchmarkInfo['stand_and_reach'] ) style={{'background:#F65E5D;text-align:center'}} @elseif(($retestInfo['stand_and_reach'] == $benchmarkInfo['stand_and_reach'])) style={{'background:#FAFB57;text-align:center'}}  @endif>							
										@if($retestInfo['stand_and_reach'] == 0) 
										{{$benchmarkInfo['stand_and_reach']}}
										@else
										{{$retestInfo['stand_and_reach'] - $benchmarkInfo['stand_and_reach']}}
										@endif
									</td>
									<td class="goal-td-bg" style="text-align:center">@if(isset($goalInfo['stand_and_reach'])){{$goalInfo['stand_and_reach']}}@endif</td>
								</tr>

								<tr>
									<td style="text-align:center">HTPS- R (sec)</td>
									<td style="text-align:center">{{$benchmarkInfo['htps']}}</td>
									<td style="text-align:center">{{$retestInfo['htps']}}</td>
									<td @if($retestInfo['htps'] == 0) style={{'background:#FAFB57;text-align:center'}} @elseif($retestInfo['htps'] < $benchmarkInfo['htps']) style={{'background:#95D195;text-align:center'}} @elseif($retestInfo['htps'] > $benchmarkInfo['htps'] ) style={{'background:#F65E5D;text-align:center'}} @elseif(($retestInfo['htps'] == $benchmarkInfo['htps'])) style={{'background:#FAFB57;text-align:center'}}  @endif>									
										@if($retestInfo['htps'] == 0) 
										{{$benchmarkInfo['htps']}}
										@else
										{{$retestInfo['htps'] - $benchmarkInfo['htps']}}
										@endif
									</td>
									<td class="goal-td-bg" style="text-align:center">@if(isset($goalInfo['htps'])){{$goalInfo['htps']}}@endif</td>
								</tr>
								
							</tbody>
						</table>	
					</div>		
				</div>
			</div>
		</div>
	</div>
</section>

@include("AdminPanel.inc.footer");

<script>
	function printCompareRetest(){
		
		let myWindow = window.open('');
			var is_chrome = Boolean(myWindow.chrome);
			myWindow.document.write('<html><head>');
			myWindow.document.write('<link href="{{asset('public/css/bootstrap.min.css')}}" rel="stylesheet" media="screen" /><link href="{{asset('public/UserPanel/custom.css')}}" rel="stylesheet" /></head><body >');
			myWindow.document.write(document.getElementById('print').innerHTML);
			myWindow.document.write('</body></html>');
			myWindow.document.close();
			
			myWindow.onload = function () { // wait until all resources loaded
				myWindow.focus(); // necessary for IE >= 10
				myWindow.print();  // change window to mywindow
				myWindow.close();// change window to mywindow
			};
			
	}
</script>
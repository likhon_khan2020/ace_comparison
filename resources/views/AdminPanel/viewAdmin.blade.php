@include('AdminPanel.inc.header')
<body style="background-color: lightgray;">
<section class="view-admin-section">
	
	@include("AdminPanel.inc.menu")

	<div class="container view-admin">
		<div class="row">			
			<div class="col-sm-12 col-md-12 view-admin-table-col">
				<div class="view-admin-header">
					<span class="view-admin-header-title">Admin <strong>Management</strong></span>
					<!--<a href="#" style="float: right;padding: 2px 4px;border-radius: 2px;color: #000;background: #fff;    position: relative;right: 5%;top: 5px;"><i class="fa fa-file" style="color:blue;"></i> Export to PDF</a>-->
					<a href="{{route('addAdmin')}}" class="btn btn-primary custom-btn" style="float: right;border-radius: 2px;color: #fff; position: relative;right: 5%;font-family: Oswald;">Add New Admin</a>
				</div>
				<div class="table-responsive view-admin-table-main">
					<table class="table view-admin-table" style="border-top: 2px solid #fff;min-width:620px;">				 
						<tbody>
							<tr>
							<th>#</th>
							<th>Name</th>
							<th>Created Date</th>
							<th>Created Time</th>
							<th>Role</th>
							<th>Status</th>
							<th>Action</th>
							</tr>
							@if(isset($adminList))
								@php
								$i = 1;
								@endphp
							@foreach($adminList as $key=>$val)
							<tr>
								<td>{{$i}}</td>
								<td style="padding-top:10px;"><img src="@if($val->filename != ''){{asset('public/uploads/admin')}}/{{$val->filename}}@else{{asset('public/img/default_user.jpg')}}@endif" style="border-radius: 50%; max-height: 40px;height: 40px;width: 40px;"/> &nbsp {{$val->first_name.' '.$val->last_name}}</td>
								<td>{{substr($val->inserted_date,0,10)}}</td>
								<td>{{substr($val->inserted_date,11,8)}}</td>
								<td>Admin</td>
								<td>@if($val->is_active == 1) <div class='active-mini-button text-center'></div> Active @else <div class='inactive-mini-button text-center'></div> Inactive @endif</td>		
								<td>
									<!--<i class="fa fa-gear" style="color:blue;"></i> --> &nbsp &nbsp
									@if($val->is_active == 1)
									<i onclick="enableDisableAdmin({{$val->id}},'inactive')" class="fa fa-times-circle" style="color:red;cursor: pointer;"></i>
									@else
									<i onclick="enableDisableAdmin({{$val->id}},'active')" class="fa fa-check" style="color:green;cursor: pointer;"></i>
									@endif
								</td>
							</tr>
								@php
								$i++;
								@endphp
							@endforeach
							@endif
						
							
						</tbody>
					</table>
				</div>
				<div class="pagination-area mb-15 mb-sm-5 mb-lg-0" style="margin-left: 50px;">
					<nav aria-label="Page navigation example">
					@include('AdminPanel/viewAdminPagination')	
					</nav>
                </div>	
				<br>
			</div>		
		</div>
	</div>
	
	@include('AdminPanel.inc/footer')


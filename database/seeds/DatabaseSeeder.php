<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(GoalTableSeeder::class);
         $this->call(AdminTableSeeder::class);
         $this->call(LevelTableSeeder::class);
    }
}

<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin')->insert([
            'user_name' => 'admin',
            'first_name' => 'Admin',
            'last_name' => "",
            'email' => "admin@gmail.com",
            'filename' => "admin.jpg",
            'is_active' => 1,
            'password' => md5('123456'),
        ]);
    }
}

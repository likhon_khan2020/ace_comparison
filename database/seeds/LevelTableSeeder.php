<?php

use Illuminate\Database\Seeder;

class LevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('level')->insert([
			[
			'id' => 1,
			'level_name' => 'Professional'
			], 
			[
			'id' => 2,
			'level_name' => 'Division 1'
			], 
			[
			'id' => 3,
			'level_name' => 'Division 2/ NAIA'
			], 
			[
			'id' => 4,
			'level_name' => 'Jr. College'
			], 
            [
			'id' => 5,
			'level_name' => 'HS Varsity'
			], 
			[
			'id' => 6,
			'level_name' => '16U Athlete'
			],
			[
			'id' => 7,
			'level_name' => '15U Athlete'
			],
			[
			'id' => 8,
			'level_name' => '14U Athlete'
			],
			[
			'id' => 9,
			'level_name' => '13U Athlete'
			],
			[
			'id' => 10,
			'level_name' => '12U Athlete'
			],
			[
			'id' => 11,
			'level_name' => '11U Athlete'
			],
			[
			'id' => 12,
			'level_name' => '10U Athlete'
			],
			[
			'id' => 13,
			'level_name' => '9U Athlete'
			]
        ]);
    }
}

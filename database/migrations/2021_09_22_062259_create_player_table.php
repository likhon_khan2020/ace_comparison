<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player', function (Blueprint $table) {
            $table->id();
			$table->string('user_name', 100)->nullable();
			$table->string('first_name', 50)->nullable();
			$table->string('last_name', 50)->nullable();
            $table->string('phone', 11)->nullable();
			$table->string('email', 100)->nullable();
			$table->string('address', 255)->nullable();
			$table->string('gender', 30)->nullable();
			$table->date('birthday')->nullable();
			$table->string('sport', 100)->nullable();
			$table->string('height', 50)->nullable();
			$table->string('weight', 50)->nullable();
			$table->string('level', 50)->nullable();
			$table->string('graduation_year', 50)->nullable();
			$table->string('password', 200)->nullable();
			$table->string('filename', 200)->nullable();
            $table->tinyInteger("is_active")->default(1);           
            $table->dateTime("inserted_date")->nullable();
            $table->dateTime("updated_date")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player');
    }
}

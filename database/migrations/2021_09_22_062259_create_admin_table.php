<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin', function (Blueprint $table) {
            $table->id();
			$table->string('user_name', 100)->nullable();
			$table->string('first_name', 50)->nullable();
			$table->string('last_name', 50)->nullable();
            $table->string('phone', 11)->nullable();
			$table->string('email', 100)->nullable();
			$table->string('address', 255)->nullable();
			$table->date('dob')->nullable();
			$table->string('password', 200)->nullable();
			$table->string('filename', 200)->nullable();
            $table->tinyInteger("is_active")->default(1);           
            $table->dateTime("inserted_date")->nullable();
			$table->integer("inserted_by")->nullable();
            $table->dateTime("updated_date")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGoalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goal', function (Blueprint $table) {
            $table->id();
			$table->integer('level_id')->unique()->nullable();
			$table->double('10_yd_sprint')->default(0);
			$table->double('40_yd_sprint')->default(0);
			$table->double('60_yd_sprint')->default(0);
			$table->double('grip_strength')->default(0);
			$table->string('broad_jump')->default(0);
			$table->double('vertical_jump')->default(0);
			$table->double('ss_shoulder_flex')->default(0);			
			$table->double('pro_agility')->default(0);						
			$table->double('throwing_velocity')->default(0);
			$table->double('exit_velocity')->default(0);						
			$table->double('stand_and_reach')->default(0);			
			$table->double('htps')->default(0);	             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goal');
    }
}

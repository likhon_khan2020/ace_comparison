<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//user panel
Route::get('/','UserPanel\PlayerController@index');
Route::get('/register','UserPanel\PlayerController@register')->name('register');
Route::post('/registerPlayer','UserPanel\PlayerController@registerPlayer')->name('registerPlayer');
Route::get('/editProfile','UserPanel\PlayerController@editProfile')->name('editProfile');
Route::post('/editPlayer','UserPanel\PlayerController@editPlayer')->name('editPlayer');
Route::post('/login','UserPanel\PlayerController@login')->name('login');
Route::get('/profile','UserPanel\PlayerController@profile')->name('profile');
Route::get('/logout','UserPanel\PlayerController@logout')->name('logout');
Route::get('/test','UserPanel\PlayerController@test')->name('test');
Route::post('/registerTesting','UserPanel\PlayerController@registerTesting')->name('registerTesting');
Route::post('/editBenchmark','UserPanel\PlayerController@editBenchmark')->name('editBenchmark');
Route::get('/retest','UserPanel\PlayerController@retest')->name('retest');
Route::post('/registerReTesting','UserPanel\PlayerController@registerReTesting')->name('registerReTesting');
Route::get('/compareBenchmark','UserPanel\PlayerController@compareBenchmark')->name('compareBenchmark');
Route::post('/compareRetest','UserPanel\PlayerController@compareRetest')->name('compareRetest');
Route::get('/emailConfirmation/{player_id}','UserPanel\PlayerController@emailConfirmation');
Route::post('/passwordResetEmail','UserPanel\PlayerController@passwordResetEmail');
Route::get('/passwordReset/{email}','UserPanel\PlayerController@passwordReset');
Route::post('/passwordResetPost','UserPanel\PlayerController@passwordResetPost')->name('passwordResetPost');

//admin panel
Route::get('/admin','AdminPanel\AdminController@index');
Route::post('/adminLogin','AdminPanel\AdminController@login')->name('adminLogin');
Route::get('/adminLogout','AdminPanel\AdminController@logout')->name('adminLogout');
Route::get('/adminProfile','AdminPanel\AdminController@adminProfile')->name('adminProfile');
Route::get('/addAdmin','AdminPanel\AdminController@addAdmin')->name('addAdmin');
Route::get('/adminEmailConfirmation/{admin_email}','AdminPanel\AdminController@emailConfirmation');
Route::get('/PlayerEmailConfirmation/{player_email}','AdminPanel\AdminController@PlayerEmailConfirmation');
Route::post('/registerAdmin','AdminPanel\AdminController@registerAdmin')->name('registerAdmin');
Route::get('/editAdminProfile','AdminPanel\AdminController@editAdminProfile')->name('editAdminProfile');
Route::post('/editAdmin','AdminPanel\AdminController@editAdmin')->name('editAdmin');
Route::get('/viewAdmin','AdminPanel\AdminController@viewAdmin')->name('viewAdmin');
Route::post('/admin/passwordResetEmail','AdminPanel\AdminController@passwordResetEmail');
Route::get('/admin/passwordReset/{email}','AdminPanel\AdminController@passwordReset');
Route::post('/admin/passwordResetPost','AdminPanel\AdminController@passwordResetPost')->name('admin.passwordResetPost');
Route::get('/enableDisableAdmin','AdminPanel\AdminController@enableDisableAdmin');
Route::get('/viewPlayer','AdminPanel\AdminController@viewPlayer')->name('viewPlayer');
Route::get('/enableDisablePlayer','AdminPanel\AdminController@enableDisablePlayer');
Route::get('/adminLogout','AdminPanel\AdminController@logout')->name('adminLogout');
Route::post('/admin/compareRetest','AdminPanel\AdminController@compareRetest')->name('admin.compareRetest');
Route::get('/playerRegister','AdminPanel\AdminController@register')->name('admin.playerRegister');
Route::post('/admin/registerPlayer','AdminPanel\AdminController@registerPlayer')->name('admin.registerPlayer');

